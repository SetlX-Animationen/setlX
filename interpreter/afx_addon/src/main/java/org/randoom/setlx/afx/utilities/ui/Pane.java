package org.randoom.setlx.afx.utilities.ui;

import javafx.scene.Node;

/**
 * The AfxWindow is spilt into multiple regions, which are all based on a JavaFX Pane Control.
 * All regions are derived from this.
 */
public interface Pane {
    Node getNode();

    double getWidth();
    double getHeight();

}
