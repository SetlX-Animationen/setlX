package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planClearCanvas(canvas):
 *      clears all previously planned draw actions for the given canvas.\\
 *      As a result the stack of planned actions is empty, resulting in a blank canvas if no new actions are added before
 *      the next draw.
 *
 *      - canvas
 *          Canvas Reference specifying for which canvas the plan should be cleared.
 *      <- \texttt{om}.
 *
 *  afx_planClearCanvas(canvas);
 *  afx_draw(canvas, 0);
 */
public class PD_afx_planClearCanvas extends PreDefinedProcedureWithRwCanvas {
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planClearCanvas();

    private PD_afx_planClearCanvas() {
        super();
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException {
        canvas.clearDrawActions();

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
