package org.randoom.setlx.afx.utilities;

public class SizedBox {
    public double x;
    public double y;
    public double w;
    public double h;

    public SizedBox(double x, double y, double w, double h){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

}
