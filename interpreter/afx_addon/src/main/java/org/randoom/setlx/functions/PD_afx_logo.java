package org.randoom.setlx.functions;

import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * JUST FOR FUN.
 */
public class PD_afx_logo extends PreDefinedProcedure {
    /** Definition of the PreDefinedProcedure `logo'. */
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_logo();

    private final static String LOGO = "\n" +
            "     >=>                                     >=======> >=>      >=>       >=>                                      >=>   " + "\n" +
            "     >=>                                     >=>        >=>   >=>           >=>                                    >=>   " + "\n" +
            "     >=>    >=> >=>  >=>     >=>    >=> >=>  >=>         >=> >=>              >=>          >=> >=>  >=>      >=> >=>>==> " + "\n" +
            "     >=>  >=>   >=>   >=>   >=>   >=>   >=>  >=====>       >=>                  >=>      >=>   >=>   >=>  >  >=>   >=>   " + "\n" +
            "     >=> >=>    >=>    >=> >=>   >=>    >=>  >=>         >=> >=>              >=>       >=>    >=>   >=> >>  >=>   >=>   " + "\n" +
            ">>   >=>  >=>   >=>     >=>=>     >=>   >=>  >=>        >=>   >=>           >=>          >=>   >=>   >=>>  >=>=>   >=>   " + "\n" +
            " >===>     >==>>>==>     >=>       >==>>>==> >=>       >=>      >=>       >=>             >==>>>==> >==>    >==>    >=>  " + "\n" + "\n";

    private PD_afx_logo() {
        super();
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) {
        state.outWrite(LOGO);
        return Om.OM;
    }

}

