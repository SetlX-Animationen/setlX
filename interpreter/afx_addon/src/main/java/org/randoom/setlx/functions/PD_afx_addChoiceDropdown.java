package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import org.randoom.setlx.afx.types.ChoiceDropDown;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.SetlList;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_addChoiceDropdown(choiceList):
 *      adds a new choice dropdown to the left pane of the afx window and returns its handle for further uses.
 *
 *      - choiceList
 *          List containing arbitrary objects which will represent the options displayed to the user.
 *      <- \texttt{om}.
 *
 * l := ["random", "sorted ascending", "sorted descending"];
 * afx_addChoiceDropdown(l);
 */
public class PD_afx_addChoiceDropdown extends PreDefinedProcedure {
    private final static ParameterDefinition CHOICE_LIST = createParameter("choiceList");
    public final static PreDefinedProcedure DEFINITION   = new PD_afx_addChoiceDropdown();

    private PD_afx_addChoiceDropdown() {
        super();
        addParameter(CHOICE_LIST);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value choiceListValue = args.get(CHOICE_LIST);
        if (!(choiceListValue instanceof SetlList)) {
            throw new IncompatibleTypeException("First argument '" + choiceListValue.toString(state) + "' is not a List.");
        }

        ObservableList<String> choiceList = FXCollections.observableArrayList();
        for (Value choiceValue : (SetlList) choiceListValue) {
            String choice = choiceValue.getUnquotedString(state);
            choiceList.add(choice);
        }

        final ChoiceBox<String> javaFxField = new ChoiceBox<>(choiceList);
        javaFxField.setMaxWidth(Double.POSITIVE_INFINITY);
        javaFxField.setValue(choiceList.get(0));

        Platform.runLater(new Runnable() {
            @Override public void run() {
                AfxWindow.waitForStartUpCompleted().getControlsPane().addNode(javaFxField);
            }
        });

        return new ChoiceDropDown(javaFxField);
    }

}
