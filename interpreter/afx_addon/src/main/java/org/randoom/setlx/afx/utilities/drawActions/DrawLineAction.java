package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;

public class DrawLineAction implements DrawAction {
    /**
     * The x-part of the top left point of the line
     */
    private final double tlx;
    /**
     * The y-part of the top left point of the line
     */
    private final double tly;
    /**
     * The x-part of the bottom right point of the line
     */
    private final double brx;
    /**
     * The y-part of the bottom right point of the line
     */
    private final double bry;
    /**
     * The desired thickness of the Line.
     * If its value is null, the thickness as set for the all strokes on the GraphicsContext is used. Otherwise, the
     *  given thickness is set temporarily.
     */
    private final Number thickness;


    public DrawLineAction(double tlx, double tly, double brx, double bry, Number thickness) {
        this.tlx = tlx;
        this.tly = tly;
        this.brx = brx;
        this.bry = bry;
        this.thickness = thickness;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        double externalLineWidth = 1.0;  // i.e. external to this Action
        if (thickness != null) {
            externalLineWidth = gc.getLineWidth();
            gc.setLineWidth((Double)thickness);
        }

        gc.strokeLine(tlx*canvasWidth, tly*canvasHeight, brx*canvasWidth, bry*canvasHeight);

        if (thickness != null) {
            gc.setLineWidth(externalLineWidth);
        }
    }

    @Override
    public DrawAction clone() {
        return new DrawLineAction(tlx, tly, brx, bry, thickness);
    }

}
