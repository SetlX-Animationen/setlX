package org.randoom.setlx.functions;

import javafx.scene.text.TextAlignment;
import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.DrawTextAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;
import java.util.Locale;

/**
 * afx_planDrawText(canvas, text, x, y, align, doFill, doOutline):
 *      plans to draw Text onto the given canvas, using Coordinates for its positioning.
 *
 *      - canvas
 *          Canvas Reference onto which the Text should be drawn.
 *      - text
 *          String, representing the actual text to be drawn.
 *      - x / y
 *          Number, representing the $x$ and $y$ coordinate of the Text.\\
 *          If the Text is centered, the coordinates specify its center point. If it is left aligned, the coordinates specify
 *          its upper left corner. If it is right aligned, the coordinates specify its upper right corner.
 *      - align
 *          String, representing the alignment of the Text.\\
 *          Possible values are \texttt{"CENTER"}, \texttt{"LEFT"} or \texttt{"RIGHT"}.\\
 *          Defaults to \texttt{"CENTER"}
 *      - doFill
 *          Boolean, specifying if the drawn Text should be filled.\\
 *          Defaults to \texttt{true}.
 *      - doOutline
 *          Boolean, specifying if the drawn Text should be outlined.\\
 *          Defaults to \texttt{false}.
 *      <- \texttt{om}.
 *
 *  PD_afx_planDrawText(canvas, "Hello World!", 0.25, 0.5);
 *  PD_afx_planDrawText(canvas, "Hello World!", 0.45, 0.5, "LEFT");
 *  PD_afx_planDrawText(canvas, "Hello World!", 0.65, 0.5, "CENTER", false, true);
 */
public class PD_afx_planDrawText extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition TEXT        = createParameter ("text");
    private final static ParameterDefinition X           = createParameter ("x");
    private final static ParameterDefinition Y           = createParameter ("y");
    private final static ParameterDefinition ALIGN       = createOptionalParameter ("align", new SetlString("CENTER"));
    private final static ParameterDefinition DOFILL      = createOptionalParameter ("doFill", SetlBoolean.TRUE);
    private final static ParameterDefinition DOOUTLINE   = createOptionalParameter ("doOutline", SetlBoolean.FALSE);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planDrawText();

    private PD_afx_planDrawText() {
        super();
        addParameter(TEXT);
        addParameter(X);
        addParameter(Y);
        addParameter(ALIGN);
        addParameter(DOFILL);
        addParameter(DOOUTLINE);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException {
        //region parse arguments
        final Value textValue = args.get(TEXT);
        if (!(textValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Second argument '" + textValue.toString(state) + "' is not a string.");
        }
        final String text = textValue.getUnquotedString(state);

        final Value xValue = args.get(X);
        if (!(xValue instanceof SetlDouble || xValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + xValue.toString(state) + "' is not a number.");
        }
        final double x = xValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(x >= 0 && x <= 1)) {
            throw new IncompatibleTypeException("Third argument '" + xValue + "' is not between 0.0 and 1.0.");
        }

        final Value yValue = args.get(Y);
        if (!(yValue instanceof SetlDouble || yValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + yValue.toString(state) + "' is not a number.");
        }
        final double y = yValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(y >= 0 && y <= 1)) {
            throw new IncompatibleTypeException("Fourth argument '" + yValue + "' is not between 0.0 and 1.0.");
        }

        final Value alignValue = args.get(ALIGN);
        if (!(alignValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Fifth argument '" + alignValue.toString(state) + "' is not a string.");
        }
        final TextAlignment align;
        switch (alignValue.getUnquotedString(state).toUpperCase(Locale.ENGLISH)) {
            case "CENTER":
                align = TextAlignment.CENTER;
                break;
            case "LEFT":
                align = TextAlignment.LEFT;
                break;
            case "RIGHT":
                align = TextAlignment.RIGHT;
                break;
            default:
                throw new IncompatibleTypeException("Fifth argument '" + alignValue.toString(state) + "' is not valid.");
        }

        final Value doFillValue = args.get(DOFILL);
        if (!(doFillValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Sixth argument '" + doFillValue.toString(state) + "' is not a boolean.");
        }
        final boolean doFill = doFillValue.equalTo(SetlBoolean.TRUE);

        final Value doOutlineValue = args.get(DOOUTLINE);
        if (!(doOutlineValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Seventh argument '" + doOutlineValue.toString(state) + "' is not a boolean.");
        }
        final boolean doOutline = doOutlineValue.equalTo(SetlBoolean.TRUE);
        //endregion

        canvas.addDrawAction(new DrawTextAction(text, x, y, align, doFill, doOutline));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
