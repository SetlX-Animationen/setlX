package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;
import org.randoom.setlx.afx.utilities.OutlinePosition;
import org.randoom.setlx.afx.utilities.OutlinePositionHelper;
import org.randoom.setlx.afx.utilities.SizedBox;

public class DrawCircleAction implements DrawAction {
    /**
     * The x-part of the top left corner of the circle's bounding box
     */
    private final double tlx;
    /**
     * The y-part of the top left corner of the circle's bounding box
     */
    private final double tly;
    private final double diameter;
    private final boolean doFill;
    private final boolean doOutline;
    private final OutlinePosition outlinePosition;


    public DrawCircleAction(
            double tlx, double tly, double diameter,
            boolean doFill, boolean doOutline, OutlinePosition outlinePosition
    ) {
        this.tlx = tlx;
        this.tly = tly;
        this.diameter = diameter;
        this.doFill = doFill;
        this.doOutline = doOutline;
        this.outlinePosition = outlinePosition;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        double cTlx = tlx*canvasWidth;
        double cTly = tly*canvasHeight;
        double cDiameter = diameter*canvasWidth;

        if (doFill) {
            gc.fillOval(cTlx, cTly, cDiameter, cDiameter);
        }

        if (doOutline) {
            final SizedBox strokeBox = OutlinePositionHelper.calculate(outlinePosition, gc, cTlx, cTly, cDiameter, cDiameter);
            gc.strokeOval(strokeBox.x, strokeBox.y, strokeBox.w, strokeBox.h);
        }
    }

    @Override
    public DrawAction clone() {
        return new DrawCircleAction(tlx, tly, diameter, doFill, doOutline, outlinePosition);
    }

}
