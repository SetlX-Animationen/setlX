package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.scene.control.TextField;
import org.randoom.setlx.afx.types.TextInput;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_addTextInput():
 *      adds a new Text - Input Field to the left pane of the AFX window.
 *
 *      <- a Handle to the newly created Input which can be used to read out its state.
 *
 *  afx_addTextInput();
 */
public class PD_afx_addTextInput extends PreDefinedProcedure {
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_addTextInput();

    private PD_afx_addTextInput() {
        super();
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) {
        final TextField javaFxField = new TextField("");

        Platform.runLater(new Runnable() {
            @Override public void run() {
                AfxWindow.waitForStartUpCompleted().getControlsPane().addNode(javaFxField);
            }
        });

        return new TextInput(javaFxField);
    }

}
