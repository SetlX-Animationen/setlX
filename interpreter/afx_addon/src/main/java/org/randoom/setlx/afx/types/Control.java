package org.randoom.setlx.afx.types;


import org.randoom.setlx.types.Value;

/**
 * An Abstract Base Class for Wrapper Types for JavaFX Controls
 */
public abstract class Control extends Value {
    public abstract javafx.scene.control.Control getControl();

    public void setDisable(boolean isDisabled) {
        getControl().setDisable(isDisabled);
    }

}
