package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.DrawLineAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Rational;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planDrawLine(canvas, tlx, tly, brx, bry, thickness):
 *      plans to draw a Line onto the given canvas, using Coordinates for the upper-left and lower-right corners.
 *
 *      - canvas
 *          Canvas Reference onto which the Line should be drawn.
 *      - tlx / tly
 *          Number, representing the $x$ and $y$ Coordinate of the upper left point of the Line.
 *      - brx / bry
 *          Number, representing the $x$ and $y$ Coordinate of the bottom right point of the Line.
 *      - thickness
 *          Number, representing the width of the Line in pixels.\\
 *          If it is not specified, the width as set for the Canvas in general, via \texttt{afx\_planSetLineWidth}, is
 *          used.
 *      <- \texttt{om}.
 *
 *  afx_planDrawLine(canvas, 0.1, 0.1, 0.25, 0.25);
 *  afx_planDrawLine(canvas, 0.6, 0.6, 0.25, 0.25, 10);
 */
public class PD_afx_planDrawLine extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition TLX         = createParameter ("tlx");
    private final static ParameterDefinition TLY         = createParameter ("tly");
    private final static ParameterDefinition BRX         = createParameter ("brx");
    private final static ParameterDefinition BRY         = createParameter ("bry");
    private final static ParameterDefinition THICKNESS   = createOptionalParameter ("thickness", Om.OM);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planDrawLine();

    private PD_afx_planDrawLine() {
        super();
        addParameter(TLX);
        addParameter(TLY);
        addParameter(BRX);
        addParameter(BRY);
        addParameter(THICKNESS);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        //region parse arguments
        final Value tlxValue = args.get(TLX);
        if (!(tlxValue instanceof SetlDouble || tlxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not a number.");
        }
        final double tlx = tlxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tlx >= 0 && tlx <= 1)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value tlyValue = args.get(TLY);
        if (!(tlyValue instanceof SetlDouble || tlyValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not a number.");
        }
        final double tly = tlyValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tly >= 0 && tly <= 1)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value brxValue = args.get(BRX);
        if (!(brxValue instanceof SetlDouble || brxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + brxValue.toString(state) + "' is not a number.");
        }
        final double brx = brxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(brx >= 0 && brx <= 1)) {
            throw new IncompatibleTypeException("Fourth argument '" + brxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value bryValue = args.get(BRY);
        if (!(bryValue instanceof SetlDouble || bryValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fifth argument '" + bryValue.toString(state) + "' is not a number.");
        }
        final double bry = bryValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(bry >= 0 && bry <= 1)) {
            throw new IncompatibleTypeException("Fifth argument '" + bryValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value thicknessValue = args.get(THICKNESS);
        final Number thickness;
        if (thicknessValue.equals(Om.OM)) {
            thickness = null;
        } else {
            if (!(thicknessValue instanceof SetlDouble || thicknessValue instanceof Rational)) {
                throw new IncompatibleTypeException("Sixth argument '" + thicknessValue.toString(state) + "' is not a number.");
            }
            double thicknessDouble = thicknessValue.toJDoubleValue(state);
            if (!(thicknessDouble > 0)) {
                throw new IncompatibleTypeException("Sixth argument '" + thicknessValue.toString(state) + "' is not greater then 0.0.");
            }
            thickness = thicknessDouble;
        }
        //endregion

        canvas.addDrawAction(new DrawLineAction(tlx, tly, brx, bry, thickness));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
