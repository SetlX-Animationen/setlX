package org.randoom.setlx.afx.utilities;

public enum OutlinePosition {
    MIDDLE,
    OUTSIDE,
    INSIDE
}
