package org.randoom.setlx.afx.types;

import javafx.scene.control.TextField;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.CodeFragment;
import org.randoom.setlx.utilities.State;

import java.util.Objects;

/**
 * A Wrapper Type for a JavaFX TextInput
 */
public class TextInput extends Input {
    private TextField javaFxControl;

    public TextInput(TextField javaFxControl){
        this.javaFxControl = javaFxControl;
    }


    @Override
    public TextField getControl(){
        return javaFxControl;
    }

    @Override
    public Value getValue() {
        return new SetlString(javaFxControl.getText());
    }

    @Override
    public void clearValue() {
        javaFxControl.setText("");
    }


    @Override
    public Value clone() {
        return new TextInput(javaFxControl);
    }

    @Override
    public void appendString(State state, StringBuilder sb, int tabs) {
        sb.append("TextInput");
    }

    @Override
    public int compareTo(CodeFragment other) {
        return (this.compareToOrdering() < other.compareToOrdering())? -1 : 1;
    }

    private final static long COMPARE_TO_ORDER_CONSTANT = generateCompareToOrderConstant(TextInput.class);

    @Override
    public long compareToOrdering() {
        return COMPARE_TO_ORDER_CONSTANT;
    }

    @Override
    public boolean equalTo(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TextInput input = (TextInput)o;
        return Objects.equals(javaFxControl, input.javaFxControl);
    }

    @Override
    public int hashCode() {
        return javaFxControl != null ? javaFxControl.hashCode() : 0;
    }
}
