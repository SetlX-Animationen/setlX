package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;
import org.randoom.setlx.afx.utilities.OutlinePosition;
import org.randoom.setlx.afx.utilities.OutlinePositionHelper;
import org.randoom.setlx.afx.utilities.SizedBox;

public class DrawEllipseAction implements DrawAction {
    /**
     * The x-part of the top left corner of the ellipse's bounding box
     */
    private final double tlx;
    /**
     * The y-part of the top left corner of the ellipse's bounding box
     */
    private final double tly;
    private final double widthAtCenter;
    private final double heightAtCenter;
    private final boolean doFill;
    private final boolean doOutline;
    private final OutlinePosition outlinePosition;


    public DrawEllipseAction(
            double tlx, double tly, double widthAtCenter, double heightAtCenter,
            boolean doFill, boolean doOutline, OutlinePosition outlinePosition
    ) {
        this.tlx = tlx;
        this.tly = tly;
        this.widthAtCenter = widthAtCenter;
        this.heightAtCenter = heightAtCenter;
        this.doFill = doFill;
        this.doOutline = doOutline;
        this.outlinePosition = outlinePosition;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        double cTlx = tlx*canvasWidth;
        double cTly = tly*canvasHeight;
        double cWidth = widthAtCenter*canvasWidth;
        double cHeight = heightAtCenter*canvasHeight;

        if (doFill) {
            gc.fillOval(cTlx, cTly, cWidth, cHeight);
        }

        if (doOutline) {
            final SizedBox strokeBox = OutlinePositionHelper.calculate(outlinePosition, gc, cTlx, cTly, cWidth, cHeight);
            gc.strokeOval(strokeBox.x, strokeBox.y, strokeBox.w, strokeBox.h);
        }
    }

    @Override
    public DrawAction clone() {
        return new DrawEllipseAction(tlx, tly, widthAtCenter, heightAtCenter, doFill, doOutline, outlinePosition);
    }

}
