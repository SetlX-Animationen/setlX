package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.ui.SliderPane;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Rational;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_draw(canvas, sleepMillsAfterDraw):
 *      executes the draw calls planned up to this point. This does not modify or clear the stack of planned actions.\\
 *      Additionally, to give the user a chance of seeing the animated movement, the execution gets halted for the
 *      specified time.
 *
 *      - canvas
 *          Canvas Reference specifying which planned draw actions should be executed.
 *      - sleepMillsAfterDraw
 *          Number representing the amount of Milliseconds to wait before continuing the execution, i.e. between the
 *          current and the next draw call.\\
 *          This value gets scaled according to the setting of the speed slider at the bottom of the AFX window.
 *      <- \texttt{om}.
 *
 *  afx_draw(canvas, 100);
 */
public class PD_afx_draw extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition SLEEPMILLSAFTERDRAW = createParameter ("sleepMillsAfterDraw");
    public final static PreDefinedProcedure DEFINITION           = new PD_afx_draw();

    private PD_afx_draw() {
        super();
        addParameter(SLEEPMILLSAFTERDRAW);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(State state, HashMap<ParameterDefinition, Value> args, Canvas canvas) throws SetlException {
        final Value sleepMillsAfterDrawVelaue = args.get(SLEEPMILLSAFTERDRAW);
        if (!(sleepMillsAfterDrawVelaue instanceof SetlDouble || sleepMillsAfterDrawVelaue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + sleepMillsAfterDrawVelaue.toString(state) + "' is not a number.");
        }
        final int sleepMillsAfterDraw = sleepMillsAfterDrawVelaue.toJIntValue(state);

        final SliderPane sliderPane = AfxWindow.waitForStartUpCompleted().getSliderPane();

        sliderPane.waitIfPaused();

        canvas.draw(state);

        try {
            Thread.sleep(
                (long)( sleepMillsAfterDraw * (1 / sliderPane.getSpeedFactor()) )
            );
        } catch (InterruptedException ex) {
            state.errWriteLn("Sleep after Draw was interrupted, continuing immediately.");
        }

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
