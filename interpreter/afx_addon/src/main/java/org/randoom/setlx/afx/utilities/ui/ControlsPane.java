package org.randoom.setlx.afx.utilities.ui;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class ControlsPane implements Pane {
    private StackPane root;
    private VBox box;
    private StackPane blocker;


    ControlsPane() {
        root = new StackPane();
        root.setPrefWidth(StyleConstants.definitiveLeftBoxWidth);
        root.setMaxWidth(StyleConstants.definitiveLeftBoxWidth);
        root.setMinWidth(StyleConstants.definitiveLeftBoxWidth);

        ScrollPane pane = new ScrollPane();
        pane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        pane.setStyle(StyleConstants.lightGrayBackgroundStyle);
        pane.setFitToWidth(true);
        root.getChildren().add(pane);

        box = new VBox();
        box.setPadding(StyleConstants.onAllSidesSpacingInset);
        box.setSpacing(StyleConstants.spacingSize);
        box.setStyle(StyleConstants.lightGrayBackgroundStyle);
        box.setFillWidth(true);
        pane.setContent(box);

        blocker = new StackPane();
        blocker.setStyle(StyleConstants.darkGrayBackgroundStyle);
        blocker.setOpacity(0.3);
        blocker.setVisible(false);
        root.getChildren().add(blocker);
    }


    @Override
    public Node getNode() {
        return root;
    }

    @Override
    public double getWidth() {
        return root.getWidth();
    }

    @Override
    public double getHeight() {
        return root.getHeight();
    }


    public void addNode(Node control) {
        box.getChildren().add(control);
    }

    public void removeNode(Node control) {
        box.getChildren().remove(control);
    }

    public int getControlCount() {
        return box.getChildren().size();
    }


    public void setBlocked(boolean isBlocked) {
        blocker.setVisible(isBlocked);
    }

}
