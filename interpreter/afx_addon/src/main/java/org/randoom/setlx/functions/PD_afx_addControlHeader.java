package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.afx.utilities.ui.ControlsPane;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_addControlHeader(text):
 *      adds a Header Label to the left panel of the AFX window.\\
 *      A Header Label is represented through a bigger font size, ideal for heading a grouping of Controls.
 *
 *      - text
 *          String representing the text to be displayed.
 *      <- \texttt{om}.
 *
 *  afx_addControlHeader("Styling Options:");
 */
public class PD_afx_addControlHeader extends PreDefinedProcedure {
    private final static ParameterDefinition TEXT       = createParameter("text");
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_addControlHeader();

    private PD_afx_addControlHeader() {
        super();
        addParameter(TEXT);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value textValue = args.get(TEXT);
        if (!(textValue instanceof SetlString)) {
            throw new IncompatibleTypeException("First argument '" + textValue.toString(state) + "' is not a string.");
        }
        final String text = textValue.getUnquotedString(state);

        final Label headerLabel = new Label(text);

        final ControlsPane controlsPane = AfxWindow.waitForStartUpCompleted().getControlsPane();

        String style = "-fx-font-size: 1.2em;";
        if (controlsPane.getControlCount() > 0) {
            style += " -fx-padding: 10 0 0 0;";
        }
        headerLabel.setStyle(style);
        headerLabel.setWrapText(true);
        headerLabel.setTextAlignment(TextAlignment.JUSTIFY);

        Platform.runLater(new Runnable() {
            @Override public void run() {
                controlsPane.addNode(headerLabel);
            }
        });

        return Om.OM;
    }

}
