package org.randoom.setlx.afx.utilities.ui;

import javafx.geometry.Insets;

/**
 * A sink to hold all styling definitions which are used in multiple places through the afx add-on
 */
public class StyleConstants {
    public static final double definitiveLeftBoxWidth     = 300;
    public static final double minRightBoxWidth           = 500;
    public static final double definitiveBottomBoxHeight  = 60;

    public static final String darkGrayBackgroundStyle   = generateBackgroundStyle("484b51");
    public static final String lightGrayBackgroundStyle  = generateBackgroundStyle("c1c2c4");
    public static final String errorRedBackgroundStyle   = generateBackgroundStyle("e3453b");

    public static final double spacingSize = 10;  // spacing between every element

    public static final String roundedCornersStyle = "-fx-background-radius: 5px;";


    public static final Insets onAllSidesSpacingInset = new Insets(StyleConstants.spacingSize);


    /**
     * Generates a JavaFX-CSS string which styles a control to have the given background color.
     * @param hexColor The desired background color, represented as a CSS Hex-Color
     * @return a JavaFX-CSS string usable for all controls
     */
    private static String generateBackgroundStyle(String hexColor) {
        return "-fx-background: #" + hexColor + "; -fx-background-color: #" + hexColor + ";";
        // we need both because some Controls only adhere to the first while some only to the second
    }

}
