package org.randoom.setlx.afx.utilities.drawActions;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.TextAlignment;

public class DrawTextAction implements DrawAction {
    private final String text;
    /**
     * The x-part of the center point of the text
     */
    private final double x;
    /**
     * The y-part of the center point of the text
     */
    private final double y;
    private TextAlignment align;
    private final boolean doFill;
    private final boolean doOutline;


    public DrawTextAction(String text, double x, double y, TextAlignment align, boolean doFill, boolean doOutline) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.align = align;
        this.doFill = doFill;
        this.doOutline = doOutline;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        double cX = x*canvasWidth;
        double cY = y*canvasHeight;

        gc.setTextAlign(align);
        if (align == TextAlignment.CENTER) {
            gc.setTextBaseline(VPos.CENTER);
        } else {
            gc.setTextBaseline(VPos.TOP);
        }

        if (doFill) {
            gc.fillText(text, cX, cY);
        }

        if (doOutline) {
            gc.strokeText(text, cX, cY);
        }
    }

    @Override
    public DrawAction clone() {
        return new DrawTextAction(text, x, y, align, doFill, doOutline);
    }

}
