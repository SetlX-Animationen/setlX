package org.randoom.setlx.functions;

import javafx.application.Application;
import javafx.application.Platform;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlBoolean;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_showWindow(title, doConfirmExit):
 *      initializes and shows the AFX window. All other AFX functions can only be called after this function has
 *      terminated.
 *
 *      - title
 *          String representing the text to be displayed in the window's Title-Bar.
 *      - doConfirmExit
 *          Boolean representing if the user should need to confirm the closing of the window.\\
 *          Defaults to false, i.e. closing the window without confirmation.
 *      <- \texttt{om}.
 *
 *  afx_showWindow();
 *  afx_showWindow("SetlX Animation Frame");
 *  afx_showWindow("SetlX Animation Frame", true);
 */
public class PD_afx_showWindow extends PreDefinedProcedure {
    private final static ParameterDefinition TITLE         = createOptionalParameter(
        "title", new SetlString("SetlX - AFX Output")
    );
    private final static ParameterDefinition DOCONFIRMEXIT = createOptionalParameter (
        "doConfirmExit", SetlBoolean.FALSE
    );
    public  final static PreDefinedProcedure DEFINITION = new PD_afx_showWindow();

    private PD_afx_showWindow() {
        super();
        addParameter(TITLE);
        addParameter(DOCONFIRMEXIT);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value titleValue = args.get(TITLE);
        if (!(titleValue instanceof SetlString)) {
            throw new IncompatibleTypeException("First argument '" + titleValue.toString(state) + "' is not a string.");
        }
        final String title = titleValue.getUnquotedString(state);

        final Value doConfirmExitValue = args.get(DOCONFIRMEXIT);
        if (!(doConfirmExitValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Second argument '" + doConfirmExitValue.toString(state) + "' is not a boolean.");
        }
        final boolean doConfirmExit = doConfirmExitValue.equalTo(SetlBoolean.TRUE);

        new Thread() {
            @Override
            public void run() {
                Application.launch(AfxWindow.class);
            }
        }.start();

        final AfxWindow afxWindow = AfxWindow.waitForStartUpCompleted();

        Platform.runLater(new Runnable() {
            @Override public void run() {
                afxWindow.setTitle(title);
                afxWindow.setExitHandler(state, doConfirmExit);
            }
        });

        return Om.OM;
    }

}
