package org.randoom.setlx.functions;

import javafx.scene.paint.Color;
import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.ColorConverter;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.SetFillColorAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planSetFillColor(canvas, name, alpha):
 *      plans to change the Fill Color to one of the predefined colors.
 *
 *      - canvas
 *          Canvas Reference for which the Fill Color should be set.
 *      - name
 *          String representing the name of one of the predefined colors (case insensitive).\\
 *          The predefined color names are shown in Figure \ref{fig:predefined-color-names} on page
 *          \pageref{fig:predefined-color-names}.
 *      - alpha
 *          Number representing the desired Alpha. A value of $1.0$ represents full opacity, $0.0$ represents
 *          full transparency. Its valid range is defined as follows: $0.0 \leq a \leq 1.0$\\
 *          Defaults to \texttt{1.0}.
 *      <- \texttt{om}.
 *
 *  afx_planSetFillColor(canvas, "FORESTGREEN");
 *  afx_planSetFillColor(canvas, "FORESTGREEN", 0.7);
 */
public class PD_afx_planSetFillColor extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition NAME       = createParameter("name");
    private final static ParameterDefinition ALPHA      = createOptionalParameter ("alpha", SetlDouble.ONE);
    public  final static PreDefinedProcedure DEFINITION = new PD_afx_planSetFillColor();

    private PD_afx_planSetFillColor() {
        super();
        addParameter(NAME);
        addParameter(ALPHA);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        final Value nameValue = args.get(NAME);
        if (!(nameValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Second argument '" + nameValue.toString(state) + "' is not a string.");
        }

        final Value alphaValue = args.get(ALPHA);
        if (!(alphaValue instanceof SetlDouble || alphaValue instanceof Rational)) {
            throw new IncompatibleTypeException("Sixth argument '" + alphaValue.toString(state) + "' is not a number.");
        }
        final double alpha = alphaValue.toJDoubleValue(state);
        if (!(alpha >= 0)) {
            throw new IncompatibleTypeException("Sixth argument '" + alphaValue.toString(state) + "' is not greater then 0.0.");
        }

        Color color;
        try {
            color = ColorConverter.stringToJavaFxColor(nameValue.getUnquotedString(state), alpha);
        } catch (IllegalArgumentException ex) {
            throw new IncompatibleTypeException("The given name does not specify a standard color.");
        }

        canvas.addDrawAction(new SetFillColorAction(color));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
