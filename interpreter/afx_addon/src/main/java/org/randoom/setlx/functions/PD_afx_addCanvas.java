package org.randoom.setlx.functions;

import javafx.application.Platform;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.afx.utilities.ui.CanvasPane;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Rational;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

/**
 * afx_addCanvas(heightFactor):
 *      adds a new canvas to the right AFX window panel, enabling draw actions on it.
 *
 *      - heightFactor
 *          Double representing the desired relation between the canvas' width and its height. The exact relation is
 *          defined as follows:
 *          $$\frac{\mbox{height}}{\mbox{width}} = \mbox{heightFactor} \mbox{ , }
 *          \mbox{height} = \mbox{width}*\mbox{heightFactor}$$
 *      <- a Canvas, which needs to be passed like a reference to calls of draw functions.
 *
 *  canvas := afx_addCanvas(1/4);
 *  afx_planDrawTextCentered(canvas, "Hello World!", 0.5, 0.5);
 */
public class PD_afx_addCanvas extends PreDefinedProcedure {
    private final static ParameterDefinition HEIGHTFACTOR = createParameter ("heightFactor");
    public  final static PreDefinedProcedure DEFINITION   = new PD_afx_addCanvas();

    private PD_afx_addCanvas() {
        super();
        addParameter(HEIGHTFACTOR);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args)
            throws SetlException {
        final Value heigthFactorValue = args.get(HEIGHTFACTOR);
        if (!(heigthFactorValue instanceof SetlDouble || heigthFactorValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + heigthFactorValue.toString(state) + "' is not a number.");
        }
        final double heightFactor = heigthFactorValue.toJDoubleValue(state);

        final CanvasPane canvasPane = AfxWindow.waitForStartUpCompleted().getCanvasPane();

        final javafx.scene.canvas.Canvas javaFxCanvas = new javafx.scene.canvas.Canvas();
        final org.randoom.setlx.afx.types.Canvas afxCanvas = new org.randoom.setlx.afx.types.Canvas(
                javaFxCanvas, heightFactor, true
        );

        final CountDownLatch latch = new CountDownLatch(1);

        Platform.runLater(new Runnable() {
            @Override public void run() {
                canvasPane.addCanvas(javaFxCanvas, heightFactor);
                canvasPane.resizeAllCanvases();
                afxCanvas.draw(state);  // set basic, empty styling

                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            // ignore
        }

        return afxCanvas;
    }

}
