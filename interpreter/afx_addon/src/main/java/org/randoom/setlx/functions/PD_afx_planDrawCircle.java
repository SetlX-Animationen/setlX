package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.OutlinePosition;
import org.randoom.setlx.afx.utilities.OutlinePositionHelper;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.DrawCircleAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planDrawCircle(canvas, tlx, tly, diameter, doFill, doOutline, outlinePosition):
 *      plans to draw a Circle onto the given canvas, using Coordinates for the upper-left corner and a size for its
 *      diameter.
 *
 *      - canvas
 *          Canvas Reference onto which the Circle should be drawn.
 *      - tlx / tly
 *          Number, representing the $x$ and $y$ Coordinate of the upper left corner of the enclosing area, into which
 *          the Circle will be drawn.
 *      - diameter
 *          Number, representing the width of the Circle at its center.\\
 *          It is scaled according to the Canvas width.
 *      - doFill
 *          Boolean, specifying if the drawn Circle should be filled.\\
 *          Defaults to \texttt{true}.
 *      - doOutline
 *          Boolean, specifying if the drawn Circle should be outlined.\\
 *          Defaults to \texttt{false}.
 *      - outlinePosition
 *          String, specifying where the outline-stroke should be placed, relative to the boundary of the Circle.\\
 *          Possible Values: \texttt{"MIDDLE"}, \texttt{"OUTSIDE"}, \texttt{"INSIDE"}.\\
 *          Defaults to \texttt{"MIDDLE"}.
 *      <- \texttt{om}.
 *
 *  afx_planDrawCircle(canvas, 0.25, 0.25, 0.25);
 *  afx_planDrawCircle(canvas, 0.75, 0.75, 0.25, false, true);
 *  afx_planDrawCircle(canvas, 0.75, 0.75, 0.25, false, true, "MIDDLE");
 */
public class PD_afx_planDrawCircle extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition TLX         = createParameter ("tlx");
    private final static ParameterDefinition TLY         = createParameter ("tly");
    private final static ParameterDefinition DIAMETER    = createParameter ("diameter");
    private final static ParameterDefinition DOFILL      = createOptionalParameter ("doFill", SetlBoolean.TRUE);
    private final static ParameterDefinition DOOUTLINE   = createOptionalParameter ("doOutline", SetlBoolean.FALSE);
    private final static ParameterDefinition OUTLINEPOS  = createOptionalParameter ("outlinePosition", new SetlString("MIDDLE"));
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planDrawCircle();

    private PD_afx_planDrawCircle() {
        super();
        addParameter(TLX);
        addParameter(TLY);
        addParameter(DIAMETER);
        addParameter(DOFILL);
        addParameter(DOOUTLINE);
        addParameter(OUTLINEPOS);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        //region parse arguments
        final Value tlxValue = args.get(TLX);
        if (!(tlxValue instanceof SetlDouble || tlxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not a number.");
        }
        final double tlx = tlxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tlx >= 0 && tlx <= 1)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value tlyValue = args.get(TLY);
        if (!(tlyValue instanceof SetlDouble || tlyValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not a number.");
        }
        final double tly = tlyValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tly >= 0 && tly <= 1)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value diameterValue = args.get(DIAMETER);
        if (!(diameterValue instanceof SetlDouble || diameterValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + diameterValue.toString(state) + "' is not a number.");
        }
        final double diameter = diameterValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(diameter >= 0 && diameter <= 1)) {
            throw new IncompatibleTypeException("Fourth argument '" + diameterValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value doFillValue = args.get(DOFILL);
        if (!(doFillValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Fifth argument '" + doFillValue.toString(state) + "' is not a boolean.");
        }
        final boolean doFill = doFillValue.equalTo(SetlBoolean.TRUE);

        final Value doOutlineValue = args.get(DOOUTLINE);
        if (!(doOutlineValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Sixth argument '" + doOutlineValue.toString(state) + "' is not a boolean.");
        }
        final boolean doOutline = doOutlineValue.equalTo(SetlBoolean.TRUE);

        final Value outlinePosValue = args.get(OUTLINEPOS);
        if (!(outlinePosValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Seventh argument '" + outlinePosValue.toString(state) + "' is not a string.");
        }
        final OutlinePosition outlinePosition;
        try {
            outlinePosition = OutlinePositionHelper.convert(outlinePosValue.getUnquotedString(state));
        } catch (IllegalArgumentException ex) {
            throw new IncompatibleTypeException("Seventh argument '" + outlinePosValue.toString(state) + "' is not valid.");
        }
        //endregion

        if (canvas.isCheckBounds() && !(tlx + diameter <= 1 && tly + diameter <= 1)) {
            throw new IncompatibleTypeException("Provided Sizings would be out-of-bounds.");
        }

        canvas.addDrawAction(new DrawCircleAction(tlx, tly, diameter, doFill, doOutline, outlinePosition));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
