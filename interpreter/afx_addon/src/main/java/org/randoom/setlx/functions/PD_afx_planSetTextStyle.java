package org.randoom.setlx.functions;

import javafx.scene.text.Font;
import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.SetTextFontAction;
import org.randoom.setlx.exceptions.FileNotReadableException;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

/**
 * afx_planSetTextStyle(canvas, size, fileName):
 *      plans to set the Font used to draw text.
 *
 *      - canvas
 *          Canvas Reference onto which the Rectangle should be drawn.
 *      - size
 *          Number representing the desired size to use for drawing text, in pt. Decimal places are
 *           dropped.
 *      - fileName
 *          String representing the file name of the Font File to use for drawing text, relative to
 *           the current working directory.\\
 *          TrueType Fonts (.ttf) and OpenType Fonts (.otf) are supported.\\
 *          If not specified the default, system-installed Arial Font is used instead.
 *      <- \texttt{om}.
 *
 *  afx_planSetTextStyle(canvas, 42);
 *  afx_planSetTextStyle(canvas, 21, "Andada.otf");
 */
public class PD_afx_planSetTextStyle extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition SIZE        = createParameter("size");
    private final static ParameterDefinition FILE_NAME   = createOptionalParameter("fileName", Om.OM);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planSetTextStyle();

    private PD_afx_planSetTextStyle() {
        super();
        addParameter(SIZE);
        addParameter(FILE_NAME);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        //region parse arguments
        final Value sizeValue = args.get(SIZE);
        if (!(sizeValue instanceof SetlDouble || sizeValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + sizeValue.toString(state) + "' is not a number.");
        }
        final int size = sizeValue.toJIntValue(state);

        final Value fileNameValue = args.get(FILE_NAME);
        final String fileName;
        if (fileNameValue instanceof Om) {
            fileName = null;
        } else {
            if (!(fileNameValue instanceof SetlString)) {
                throw new IncompatibleTypeException("Third argument '" + fileNameValue.toString(state) + "' is not a string.");
            }
            fileName = state.filterFileName(fileNameValue.getUnquotedString(state));
        }
        //endregion

        if (fileName == null) {
            canvas.addDrawAction(new SetTextFontAction(new Font("Arial", size)));
        } else {
            try (
                FileInputStream fileStream = new FileInputStream(fileName)
            ) {
                Font font = Font.loadFont(fileStream, size);

                if (font == null) {
                    throw new FileNotReadableException("Unable to handle Font File '" + fileName + "'.", null);
                }

                canvas.addDrawAction(new SetTextFontAction(font));
            } catch (final FileNotFoundException ex) {
                throw new FileNotReadableException("Font File '" + fileName + "' does not exist.", ex);
            } catch (final IOException ex) {
                throw new FileNotReadableException("Unable to read Font File '" + fileName + "'.", ex);
            }
        }

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
