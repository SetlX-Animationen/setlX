package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_addControlHint(text):
 *      adds a Hint Label to the left panel of the AFX window.\\
 *      The Hint will be displayed in the same font as text on controls and is meant to provide additional context.
 *
 *      - text
 *          String representing the text to be displayed.
 *      <- \texttt{om}.
 *
 *  afx_addControlHint("Please enter the number of disks to shuffle below:");
 */
public class PD_afx_addControlHint extends PreDefinedProcedure {
    private final static ParameterDefinition TEXT       = createParameter("text");
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_addControlHint();

    private PD_afx_addControlHint() {
        super();
        addParameter(TEXT);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value textValue = args.get(TEXT);
        if (!(textValue instanceof SetlString)) {
            throw new IncompatibleTypeException("First argument '" + textValue.toString(state) + "' is not a string.");
        }
        final String text = textValue.getUnquotedString(state);

        final Label hintLabel = new Label(text);
        hintLabel.setWrapText(true);
        hintLabel.setTextAlignment(TextAlignment.JUSTIFY);

        Platform.runLater(new Runnable() {
            @Override public void run() {
                AfxWindow.waitForStartUpCompleted().getControlsPane().addNode(hintLabel);
            }
        });

        return Om.OM;
    }

}
