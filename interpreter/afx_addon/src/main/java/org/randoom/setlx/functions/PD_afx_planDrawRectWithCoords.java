package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.OutlinePosition;
import org.randoom.setlx.afx.utilities.OutlinePositionHelper;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.DrawRectangleAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planDrawRectWithCoords(canvas, tlx, tly, brx, bry, doFill, doOutline, outlinePosition):
 *      plans to draw a Rectangle onto the given canvas, using Coordinates for the upper-left and lower-right corners.
 *
 *      - canvas
 *          Canvas Reference onto which the Rectangle should be drawn.
 *      - tlx / tly
 *          Number, representing the $x$ and $y$ Coordinate of the upper left corner of the Rectangle.
 *      - brx / bry
 *          Number, representing the $x$ and $y$ Coordinate of the bottom right corner of the Rectangle.
 *      - doFill
 *          Boolean, specifying if the drawn Rectangle should be filled.\\
 *          Defaults to \texttt{true}.
 *      - doOutline
 *          Boolean, specifying if the drawn Rectangle should be outlined.\\
 *          Defaults to \texttt{false}.
 *      - outlinePosition
 *          String, specifying where the outline-stroke should be placed, relative to the boundary of the Rectangle.\\
 *          Possible Values: \texttt{"MIDDLE"}, \texttt{"OUTSIDE"}, \texttt{"INSIDE"}.\\
 *          Defaults to \texttt{"MIDDLE"}.
 *      <- \texttt{om}.
 *
 *  afx_planDrawRectWithCoords(canvas, 0.25, 0.25, 0.35, 0.3);
 *  afx_planDrawRectWithCoords(canvas, 0.75, 0.75, 0.35, 0.3, false, true);
 *  afx_planDrawRectWithCoords(canvas, 0.75, 0.75, 0.35, 0.3, false, true, "INSIDE");
 */
public class PD_afx_planDrawRectWithCoords extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition TLX         = createParameter ("tlx");
    private final static ParameterDefinition TLY         = createParameter ("tly");
    private final static ParameterDefinition BRX         = createParameter ("brx");
    private final static ParameterDefinition BRY         = createParameter ("bry");
    private final static ParameterDefinition DOFILL      = createOptionalParameter ("doFill", SetlBoolean.TRUE);
    private final static ParameterDefinition DOOUTLINE   = createOptionalParameter ("doOutline", SetlBoolean.FALSE);
    private final static ParameterDefinition OUTLINEPOS  = createOptionalParameter ("outlinePosition", new SetlString("MIDDLE"));
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planDrawRectWithCoords();

    private PD_afx_planDrawRectWithCoords() {
        super();
        addParameter(TLX);
        addParameter(TLY);
        addParameter(BRX);
        addParameter(BRY);
        addParameter(DOFILL);
        addParameter(DOOUTLINE);
        addParameter(OUTLINEPOS);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException {
        //region parse arguments
        final Value tlxValue = args.get(TLX);
        if (!(tlxValue instanceof SetlDouble || tlxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not a number.");
        }
        final double tlx = tlxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tlx >= 0 && tlx <= 1)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value tlyValue = args.get(TLY);
        if (!(tlyValue instanceof SetlDouble || tlyValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not a number.");
        }
        final double tly = tlyValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tly >= 0 && tly <= 1)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value brxValue = args.get(BRX);
        if (!(brxValue instanceof SetlDouble || brxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + brxValue.toString(state) + "' is not a number.");
        }
        final double brx = brxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(brx >= 0 && brx <= 1)) {
            throw new IncompatibleTypeException("Fourth argument '" + brxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value bryValue = args.get(BRY);
        if (!(bryValue instanceof SetlDouble || bryValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fifth argument '" + bryValue.toString(state) + "' is not a number.");
        }
        final double bry = bryValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(bry >= 0 && bry <= 1)) {
            throw new IncompatibleTypeException("Fifth argument '" + bryValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value doFillValue = args.get(DOFILL);
        if (!(doFillValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Sixth argument '" + doFillValue.toString(state) + "' is not a boolean.");
        }
        final boolean doFill = doFillValue.equalTo(SetlBoolean.TRUE);

        final Value doOutlineValue = args.get(DOOUTLINE);
        if (!(doOutlineValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Seventh argument '" + doOutlineValue.toString(state) + "' is not a boolean.");
        }
        final boolean doOutline = doOutlineValue.equalTo(SetlBoolean.TRUE);

        final Value outlinePosValue = args.get(OUTLINEPOS);
        if (!(outlinePosValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Eighth argument '" + outlinePosValue.toString(state) + "' is not a string.");
        }
        final OutlinePosition outlinePosition;
        try {
            outlinePosition = OutlinePositionHelper.convert(outlinePosValue.getUnquotedString(state));
        } catch (IllegalArgumentException ex) {
            throw new IncompatibleTypeException("Eighth argument '" + outlinePosValue.toString(state) + "' is not valid.");
        }
        //endregion

        // tl
        //  O---------------.
        //  |               |
        //  .---------------O
        //                  br

        double width = brx - tlx;
        double height = bry - tly;

        canvas.addDrawAction(new DrawRectangleAction(tlx, tly, width, height, doFill, doOutline, outlinePosition));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
