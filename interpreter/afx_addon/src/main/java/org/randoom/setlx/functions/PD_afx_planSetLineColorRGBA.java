package org.randoom.setlx.functions;

import javafx.scene.paint.Color;
import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.ColorConverter;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.SetBorderColorAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Rational;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planSetLineColorRGBA(canvas, red, green, blue, alpha):
 *      plans to change the Line Color to the one defined by the given RGB+A values.
 *
 *      - canvas
 *          Canvas Reference for which the Line Color should be set.
 *      - red
 *          Number representing the desired amount of Red. Its valid range is defined as follows: $0.0 \leq r \leq
 *          255.0$
 *      - green
 *          Number representing the desired amount of Green. Its valid range is defined as follows: $0.0 \leq g \leq
 *          255.0$
 *      - blue
 *          Number representing the desired amount of Blue. Its valid range is defined as follows: $0.0 \leq b \leq
 *          255.0$
 *      - alpha
 *          Number representing the desired Alpha. A value of $1.0$ represents full opacity, $0.0$ represents
 *          full transparency. Its valid range is defined as follows: $0.0 \leq a \leq 1.0$\\
 *          Defaults to \texttt{1.0}.
 *      <- \texttt{om}.
 *
 * afx_planSetLineColorRGBA(canvas, 42, 42, 42);
 * afx_planSetLineColorRGBA(canvas, 42, 42, 42, 0.7);
 */
public class PD_afx_planSetLineColorRGBA extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition RED         = createParameter("red");
    private final static ParameterDefinition GREEN       = createParameter("green");
    private final static ParameterDefinition BLUE        = createParameter("blue");
    private final static ParameterDefinition ALPHA       = createOptionalParameter ("alpha", SetlDouble.ONE);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planSetLineColorRGBA();

    private PD_afx_planSetLineColorRGBA() {
        super();
        addParameter(RED);
        addParameter(GREEN);
        addParameter(BLUE);
        addParameter(ALPHA);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        final Value redValue = args.get(RED);
        if (!(redValue instanceof SetlDouble || redValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + redValue.toString(state) + "' is not a number.");
        }
        final int red = redValue.toJIntValue(state);

        final Value greenValue = args.get(GREEN);
        if (!(greenValue instanceof SetlDouble || greenValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + greenValue.toString(state) + "' is not a number.");
        }
        final int green = greenValue.toJIntValue(state);

        final Value blueValue = args.get(BLUE);
        if (!(blueValue instanceof SetlDouble || blueValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + blueValue.toString(state) + "' is not a number.");
        }
        final int blue = blueValue.toJIntValue(state);

        final Value alphaValue = args.get(ALPHA);
        if (!(alphaValue instanceof SetlDouble || alphaValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fifth argument '" + alphaValue.toString(state) + "' is not a number.");
        }
        final double alpha = alphaValue.toJDoubleValue(state);

        Color color;
        try {
            color = ColorConverter.rgbaToJavaFxColor(red, green, blue, alpha);
        } catch (IllegalArgumentException ex) {
            throw new IncompatibleTypeException("The given values don't form a supported color.");
        }
        canvas.addDrawAction(new SetBorderColorAction(color));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
