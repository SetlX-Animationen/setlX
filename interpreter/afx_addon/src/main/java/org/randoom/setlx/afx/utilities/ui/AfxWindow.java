package org.randoom.setlx.afx.utilities.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.randoom.setlx.utilities.State;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * Base Class for a JavaFX Window utilized for afx functions
 */
public class AfxWindow extends Application {
    private static final CountDownLatch latch = new CountDownLatch(2);
    private static AfxWindow afxWindow = null;

    private Stage primaryStage;
    private StackPane root;

    private ControlsPane  controlsPane;
    public  ControlsPane  getControlsPane() { return controlsPane; }
    private CanvasPane    canvasPane;
    public  CanvasPane    getCanvasPane() { return canvasPane; }
    private SliderPane    sliderPane;
    public  SliderPane    getSliderPane() { return sliderPane; }
    private ModalPane     modalPane;
    public  ModalPane     getModalPane() { return modalPane; }

    private HashMap<KeyCode, Button> keyBinds = new HashMap<KeyCode, Button>();

    public Semaphore inputBlockedSemaphore = new Semaphore(1);


    public AfxWindow() {
        setAfxWindow(this);
    }


    public static AfxWindow waitForStartUpCompleted() {
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return afxWindow;
    }

    private static void setAfxWindow(AfxWindow base) {
        afxWindow = base;
        latch.countDown();
    }


    /**
     * Initializes the JavaFX Application.
     * Called by `Application.launch(AfxWindow.class);` (inside PD_afx_showWindow)
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        primaryStage.getIcons().add(
            new javafx.scene.image.Image(getClass().getResourceAsStream("/icon.png"))
        );
        try {
            Class<?> macOsApplicationClass = Class.forName("com.apple.eawt.Application");
            macOsApplicationClass.getMethod("setDockIconImage", java.awt.Image.class).invoke(
                macOsApplicationClass.getMethod("getApplication").invoke(null),
                new javax.swing.ImageIcon(getClass().getResource("/icon.png")).getImage()
            );
        } catch (Exception  e) {
            // this intentionally won't work on Windows or Linux
            // and if it fails on macOS, it's not a show stopper, so we don't care
        }

        root = new StackPane();

        double minWidth =
            StyleConstants.definitiveLeftBoxWidth +
            StyleConstants.minRightBoxWidth +
            3*StyleConstants.spacingSize
        ;

        Scene scene = new Scene(root, minWidth, 650);
        scene.getStylesheets().add(getClass().getResource("/scrollbars.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.setMinWidth(minWidth);
        primaryStage.setMinHeight(650);

        GridPane main = new GridPane();
        root.getChildren().add(main);

        main.setPadding(StyleConstants.onAllSidesSpacingInset);
        main.setHgap(StyleConstants.spacingSize);
        main.setVgap(StyleConstants.spacingSize);
        main.setStyle(StyleConstants.darkGrayBackgroundStyle);

        ColumnConstraints growingWidthColCons = new ColumnConstraints();
        growingWidthColCons.setHgrow(Priority.ALWAYS);
        ColumnConstraints fixedWidthColCons = new ColumnConstraints();
        fixedWidthColCons.setHgrow(Priority.NEVER);
        main.getColumnConstraints().addAll(fixedWidthColCons, growingWidthColCons);

        RowConstraints growingHeightRowCons = new RowConstraints();
        growingHeightRowCons.setVgrow(Priority.ALWAYS);
        RowConstraints fixedHeightRowCons = new RowConstraints();
        fixedHeightRowCons.setVgrow(Priority.NEVER);
        main.getRowConstraints().addAll(growingHeightRowCons, fixedHeightRowCons);

        controlsPane = new ControlsPane();
        main.add(controlsPane.getNode(), 0, 0, 1, 1);
        canvasPane = new CanvasPane();
        main.add(canvasPane.getNode(), 1, 0, 1, 1);
        sliderPane = new SliderPane();
        main.add(sliderPane.getNode(), 0, 1, 2, 1);

        modalPane = new ModalPane();
        root.getChildren().add(modalPane.getNode());

        root.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                canvasPane.resizeAllCanvases();
            }
        });
        root.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                canvasPane.resizeAllCanvases();
            }
        });

        root.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (inputBlockedSemaphore.availablePermits() == 0) {
                    return;  // ignore Key-Binds when blocked
                }

                if (keyBinds.containsKey(event.getCode())) {
                    Button button = keyBinds.get(event.getCode());
                    if (!button.isDisabled()) {
                        button.getOnAction().handle(new ActionEvent());
                    }
                }
            }
        });

        primaryStage.show();
        latch.countDown();
    }


    public void setTitle(String title) {
        primaryStage.setTitle(title);
    }


    public void addGlobalKeyBind(KeyCode kc, Button button) {
        keyBinds.put(kc, button);
    }


    public double getAllowableLeftBoxWidth() {
        return root.getWidth() - controlsPane.getWidth() - 3*StyleConstants.spacingSize;
    }

    public double getAllowableLeftBoxHeight() {
        return root.getHeight() - sliderPane.getHeight() - 3*StyleConstants.spacingSize;
    }


    public void setExitHandler(final State state, final boolean doConfirm) {
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override public void handle(WindowEvent event) {
                if (doConfirm) {
                    event.consume();

                    final CountDownLatch modalShownLatch = new CountDownLatch(1);

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            modalPane.showModal("Confirm Exit", "Close SetlX?", Arrays.asList("yes", "no"));
                            modalShownLatch.countDown();
                        }
                    });

                    Thread waitForButton = new Thread() {
                        @Override
                        public void run() {
                            try {
                                modalShownLatch.await();
                            } catch (InterruptedException e) {
                                // ignore
                            }

                            int buttonIndex = modalPane.awaitButtonClick();
                            if (buttonIndex == 0) { /* yes */
                                exitAll(state);
                            }
                        }
                    };
                    waitForButton.start();
                } else {
                    exitAll(state);
                }
            }
        });
    }


    /**
     * Terminates the current SetlX Program completely, without looking back.
     */
    private void exitAll(State state) {
        state.outWriteLn("\n" +
                "  ___                  _     ___   _  _         _                                    " + "\n" +
                " / __|  ___   ___   __| |   | _ ) | || |  ___  | |    /\"/  ___  __ __ (_) |\"|_  \\\"\\" + "\n" +
                "| (_ | / _ \\ / _ \\ / _` |   | _ \\  \\_, | / -_) |_|   | |  / -_) \\ \\ / | | |  _|  | |" + "\n" +
                " \\___| \\___/ \\___/ \\__,_|   |___/  |__/  \\___| (_)    \\_\\ \\___| /_\\_\\ |_|  \\__| /_/" + "\n"
        );
        System.exit(0);
        // Because we want to exit completely and this is the easiest way to end all possible threads
    }

}
