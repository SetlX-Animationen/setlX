package org.randoom.setlx.afx.utilities;

import javafx.scene.paint.Color;

import java.util.Locale;

/**
 * Utility for converting color identifiers back and forth.
 */
public class ColorConverter {
    /**
     * Converts the given RGB+A values to the corresponding JavaFX Color.
     *
     * @param r The red value, 0 <= r <= 255
     * @param g The green value, 0 <= g <= 255
     * @param b The blue value, 0 <= b <= 255
     * @param a The Alpha Value, i.e. the Opacity, 0.0 <= a <= 1.0, 1.0 = fully opaque
     * @return The corresponding JavaFX Color
     */
    public static Color rgbaToJavaFxColor(int r, int g, int b, double a) throws IllegalArgumentException {
        return Color.rgb(r, g, b, a);
    }

    /**
     * Converts the given HSV+A values to the corresponding JavaFX Color.
     *
     * @param h The Hue, in degrees
     * @param s The Saturation, 0.0 <= s <= 1.0
     * @param v The Value, 0.0 <= v <= 1.0
     * @param a The Alpha Value, i.e. the Opacity, 0.0 <= a <= 1.0, 1.0 = fully opaque
     * @return The corresponding JavaFX Color
     */
    public static Color hsvaToJavaFxColor(double h, double s, double v, double a) throws IllegalArgumentException {
        return Color.hsb(h, s, v, a);
    }

    /**
     * Converts a color name to the corresponding JavaFX color.
     *
     * @param name The name of the color
     * @param a    The Alpha Value, i.e. the Opacity, 0.0 <= a <= 1.0, 1.0 = fully opaque
     * @return The corresponding JavaFX Color
     */
    public static Color stringToJavaFxColor(String name, double a) throws IllegalArgumentException {
        Color color;

        switch (name.toUpperCase(Locale.ENGLISH)) {
            case "TRANSPARENT":
                color = Color.TRANSPARENT;
                break;
            case "ALICEBLUE":
                color = Color.ALICEBLUE;
                break;
            case "ANTIQUEWHITE":
                color = Color.ANTIQUEWHITE;
                break;
            case "AQUA":
                color = Color.AQUA;
                break;
            case "AQUAMARINE":
                color = Color.AQUAMARINE;
                break;
            case "AZURE":
                color = Color.AZURE;
                break;
            case "BEIGE":
                color = Color.BEIGE;
                break;
            case "BISQUE":
                color = Color.BISQUE;
                break;
            case "BLACK":
                color = Color.BLACK;
                break;
            case "BLANCHEDALMOND":
                color = Color.BLANCHEDALMOND;
                break;
            case "BLUE":
                color = Color.BLUE;
                break;
            case "BLUEVIOLET":
                color = Color.BLUEVIOLET;
                break;
            case "BROWN":
                color = Color.BROWN;
                break;
            case "BURLYWOOD":
                color = Color.BURLYWOOD;
                break;
            case "CADETBLUE":
                color = Color.CADETBLUE;
                break;
            case "CHARTREUSE":
                color = Color.CHARTREUSE;
                break;
            case "CHOCOLATE":
                color = Color.CHOCOLATE;
                break;
            case "CORAL":
                color = Color.CORAL;
                break;
            case "CORNFLOWERBLUE":
                color = Color.CORNFLOWERBLUE;
                break;
            case "CORNSILK":
                color = Color.CORNSILK;
                break;
            case "CRIMSON":
                color = Color.CRIMSON;
                break;
            case "CYAN":
                color = Color.CYAN;
                break;
            case "DARKBLUE":
                color = Color.DARKBLUE;
                break;
            case "DARKCYAN":
                color = Color.DARKCYAN;
                break;
            case "DARKGOLDENROD":
                color = Color.DARKGOLDENROD;
                break;
            case "DARKGRAY":
                color = Color.DARKGRAY;
                break;
            case "DARKGREEN":
                color = Color.DARKGREEN;
                break;
            case "DARKGREY":
                color = Color.DARKGREY;
                break;
            case "DARKKHAKI":
                color = Color.DARKKHAKI;
                break;
            case "DARKMAGENTA":
                color = Color.DARKMAGENTA;
                break;
            case "DARKOLIVEGREEN":
                color = Color.DARKOLIVEGREEN;
                break;
            case "DARKORANGE":
                color = Color.DARKORANGE;
                break;
            case "DARKORCHID":
                color = Color.DARKORCHID;
                break;
            case "DARKRED":
                color = Color.DARKRED;
                break;
            case "DARKSALMON":
                color = Color.DARKSALMON;
                break;
            case "DARKSEAGREEN":
                color = Color.DARKSEAGREEN;
                break;
            case "DARKSLATEBLUE":
                color = Color.DARKSLATEBLUE;
                break;
            case "DARKSLATEGRAY":
                color = Color.DARKSLATEGRAY;
                break;
            case "DARKSLATEGREY":
                color = Color.DARKSLATEGREY;
                break;
            case "DARKTURQUOISE":
                color = Color.DARKTURQUOISE;
                break;
            case "DARKVIOLET":
                color = Color.DARKVIOLET;
                break;
            case "DEEPPINK":
                color = Color.DEEPPINK;
                break;
            case "DEEPSKYBLUE":
                color = Color.DEEPSKYBLUE;
                break;
            case "DIMGRAY":
                color = Color.DIMGRAY;
                break;
            case "DIMGREY":
                color = Color.DIMGREY;
                break;
            case "DODGERBLUE":
                color = Color.DODGERBLUE;
                break;
            case "FIREBRICK":
                color = Color.FIREBRICK;
                break;
            case "FLORALWHITE":
                color = Color.FLORALWHITE;
                break;
            case "FORESTGREEN":
                color = Color.FORESTGREEN;
                break;
            case "FUCHSIA":
                color = Color.FUCHSIA;
                break;
            case "GAINSBORO":
                color = Color.GAINSBORO;
                break;
            case "GHOSTWHITE":
                color = Color.GHOSTWHITE;
                break;
            case "GOLD":
                color = Color.GOLD;
                break;
            case "GOLDENROD":
                color = Color.GOLDENROD;
                break;
            case "GRAY":
                color = Color.GRAY;
                break;
            case "GREEN":
                color = Color.GREEN;
                break;
            case "GREENYELLOW":
                color = Color.GREENYELLOW;
                break;
            case "GREY":
                color = Color.GREY;
                break;
            case "HONEYDEW":
                color = Color.HONEYDEW;
                break;
            case "HOTPINK":
                color = Color.HOTPINK;
                break;
            case "INDIANRED":
                color = Color.INDIANRED;
                break;
            case "INDIGO":
                color = Color.INDIGO;
                break;
            case "IVORY":
                color = Color.IVORY;
                break;
            case "KHAKI":
                color = Color.KHAKI;
                break;
            case "LAVENDER":
                color = Color.LAVENDER;
                break;
            case "LAVENDERBLUSH":
                color = Color.LAVENDERBLUSH;
                break;
            case "LAWNGREEN":
                color = Color.LAWNGREEN;
                break;
            case "LEMONCHIFFON":
                color = Color.LEMONCHIFFON;
                break;
            case "LIGHTBLUE":
                color = Color.LIGHTBLUE;
                break;
            case "LIGHTCORAL":
                color = Color.LIGHTCORAL;
                break;
            case "LIGHTCYAN":
                color = Color.LIGHTCYAN;
                break;
            case "LIGHTGOLDENRODYELLOW":
                color = Color.LIGHTGOLDENRODYELLOW;
                break;
            case "LIGHTGRAY":
                color = Color.LIGHTGRAY;
                break;
            case "LIGHTGREEN":
                color = Color.LIGHTGREEN;
                break;
            case "LIGHTGREY":
                color = Color.LIGHTGREY;
                break;
            case "LIGHTPINK":
                color = Color.LIGHTPINK;
                break;
            case "LIGHTSALMON":
                color = Color.LIGHTSALMON;
                break;
            case "LIGHTSEAGREEN":
                color = Color.LIGHTSEAGREEN;
                break;
            case "LIGHTSKYBLUE":
                color = Color.LIGHTSKYBLUE;
                break;
            case "LIGHTSLATEGRAY":
                color = Color.LIGHTSLATEGRAY;
                break;
            case "LIGHTSLATEGREY":
                color = Color.LIGHTSLATEGREY;
                break;
            case "LIGHTSTEELBLUE":
                color = Color.LIGHTSTEELBLUE;
                break;
            case "LIGHTYELLOW":
                color = Color.LIGHTYELLOW;
                break;
            case "LIME":
                color = Color.LIME;
                break;
            case "LIMEGREEN":
                color = Color.LIMEGREEN;
                break;
            case "LINEN":
                color = Color.LINEN;
                break;
            case "MAGENTA":
                color = Color.MAGENTA;
                break;
            case "MAROON":
                color = Color.MAROON;
                break;
            case "MEDIUMAQUAMARINE":
                color = Color.MEDIUMAQUAMARINE;
                break;
            case "MEDIUMBLUE":
                color = Color.MEDIUMBLUE;
                break;
            case "MEDIUMORCHID":
                color = Color.MEDIUMORCHID;
                break;
            case "MEDIUMPURPLE":
                color = Color.MEDIUMPURPLE;
                break;
            case "MEDIUMSEAGREEN":
                color = Color.MEDIUMSEAGREEN;
                break;
            case "MEDIUMSLATEBLUE":
                color = Color.MEDIUMSLATEBLUE;
                break;
            case "MEDIUMSPRINGGREEN":
                color = Color.MEDIUMSPRINGGREEN;
                break;
            case "MEDIUMTURQUOISE":
                color = Color.MEDIUMTURQUOISE;
                break;
            case "MEDIUMVIOLETRED":
                color = Color.MEDIUMVIOLETRED;
                break;
            case "MIDNIGHTBLUE":
                color = Color.MIDNIGHTBLUE;
                break;
            case "MINTCREAM":
                color = Color.MINTCREAM;
                break;
            case "MISTYROSE":
                color = Color.MISTYROSE;
                break;
            case "MOCCASIN":
                color = Color.MOCCASIN;
                break;
            case "NAVAJOWHITE":
                color = Color.NAVAJOWHITE;
                break;
            case "NAVY":
                color = Color.NAVY;
                break;
            case "OLDLACE":
                color = Color.OLDLACE;
                break;
            case "OLIVE":
                color = Color.OLIVE;
                break;
            case "OLIVEDRAB":
                color = Color.OLIVEDRAB;
                break;
            case "ORANGE":
                color = Color.ORANGE;
                break;
            case "ORANGERED":
                color = Color.ORANGERED;
                break;
            case "ORCHID":
                color = Color.ORCHID;
                break;
            case "PALEGOLDENROD":
                color = Color.PALEGOLDENROD;
                break;
            case "PALEGREEN":
                color = Color.PALEGREEN;
                break;
            case "PALETURQUOISE":
                color = Color.PALETURQUOISE;
                break;
            case "PALEVIOLETRED":
                color = Color.PALEVIOLETRED;
                break;
            case "PAPAYAWHIP":
                color = Color.PAPAYAWHIP;
                break;
            case "PEACHPUFF":
                color = Color.PEACHPUFF;
                break;
            case "PERU":
                color = Color.PERU;
                break;
            case "PINK":
                color = Color.PINK;
                break;
            case "PLUM":
                color = Color.PLUM;
                break;
            case "POWDERBLUE":
                color = Color.POWDERBLUE;
                break;
            case "PURPLE":
                color = Color.PURPLE;
                break;
            case "RED":
                color = Color.RED;
                break;
            case "ROSYBROWN":
                color = Color.ROSYBROWN;
                break;
            case "ROYALBLUE":
                color = Color.ROYALBLUE;
                break;
            case "SADDLEBROWN":
                color = Color.SADDLEBROWN;
                break;
            case "SALMON":
                color = Color.SALMON;
                break;
            case "SANDYBROWN":
                color = Color.SANDYBROWN;
                break;
            case "SEAGREEN":
                color = Color.SEAGREEN;
                break;
            case "SEASHELL":
                color = Color.SEASHELL;
                break;
            case "SIENNA":
                color = Color.SIENNA;
                break;
            case "SILVER":
                color = Color.SILVER;
                break;
            case "SKYBLUE":
                color = Color.SKYBLUE;
                break;
            case "SLATEBLUE":
                color = Color.SLATEBLUE;
                break;
            case "SLATEGRAY":
                color = Color.SLATEGRAY;
                break;
            case "SLATEGREY":
                color = Color.SLATEGREY;
                break;
            case "SNOW":
                color = Color.SNOW;
                break;
            case "SPRINGGREEN":
                color = Color.SPRINGGREEN;
                break;
            case "STEELBLUE":
                color = Color.STEELBLUE;
                break;
            case "TAN":
                color = Color.TAN;
                break;
            case "TEAL":
                color = Color.TEAL;
                break;
            case "THISTLE":
                color = Color.THISTLE;
                break;
            case "TOMATO":
                color = Color.TOMATO;
                break;
            case "TURQUOISE":
                color = Color.TURQUOISE;
                break;
            case "VIOLET":
                color = Color.VIOLET;
                break;
            case "WHEAT":
                color = Color.WHEAT;
                break;
            case "WHITE":
                color = Color.WHITE;
                break;
            case "WHITESMOKE":
                color = Color.WHITESMOKE;
                break;
            case "YELLOW":
                color = Color.YELLOW;
                break;
            case "YELLOWGREEN":
                color = Color.YELLOWGREEN;
                break;
            default:
                throw new IllegalArgumentException("The given color name is not known!");
        }

        return color.deriveColor(0, 1, 1, a);
        // hueShift, saturationFactor, brightnessFactor, opacityFactor
        // As we only want to affect the desired opacity
    }

}
