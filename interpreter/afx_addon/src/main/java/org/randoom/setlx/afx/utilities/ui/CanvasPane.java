package org.randoom.setlx.afx.utilities.ui;

import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

import java.util.ArrayList;

public class CanvasPane implements Pane {
    private VBox box;

    private ArrayList<Pair<Canvas, Number>> canvasAndHeightFactorList = new ArrayList<>();


    public CanvasPane() {
        box = new VBox();

        box.setPadding(StyleConstants.onAllSidesSpacingInset);
        box.setSpacing(StyleConstants.spacingSize);
        box.setStyle(StyleConstants.lightGrayBackgroundStyle);

        box.setMinWidth(StyleConstants.minRightBoxWidth);

        box.setFillWidth(false);
    }


    public void resizeAllCanvases() {
            AfxWindow afxWindow = AfxWindow.waitForStartUpCompleted();
            double newBoxWidth = afxWindow.getAllowableLeftBoxWidth();
            double newBoxHeight = afxWindow.getAllowableLeftBoxHeight();

            int remainingCanvases = canvasAndHeightFactorList.size();
            double allowableWidth = newBoxWidth - StyleConstants.spacingSize * 2;
            double remainingHeight = newBoxHeight - StyleConstants.spacingSize * 2;
            remainingHeight -= StyleConstants.spacingSize * (remainingCanvases - 1);

            for (Pair<Canvas, Number> pair : canvasAndHeightFactorList) {
                Canvas canvas = pair.getKey();
                double heightFactor = pair.getValue().doubleValue();

                double newCanvasHeight = Math.floor(remainingHeight / remainingCanvases);
                double newCanvasWidth = Math.floor(newCanvasHeight / heightFactor);

                if (newCanvasWidth > allowableWidth) {
                    newCanvasWidth = allowableWidth;
                    newCanvasHeight = newCanvasWidth * heightFactor;
                }

                canvas.setHeight(newCanvasHeight);
                canvas.setWidth(newCanvasWidth);

                remainingHeight -= newCanvasHeight;
                remainingCanvases--;
            }
    }


    @Override
    public Node getNode() {
        return box;
    }

    @Override
    public double getWidth() {
        return box.getWidth();
    }

    @Override
    public double getHeight() {
        return box.getHeight();
    }


    public void addCanvas(Canvas canvas, double heightFactor) {
        StackPane holder = new StackPane();
        holder.getChildren().add(canvas);
        holder.setStyle("-fx-background-color: white");
        box.getChildren().add(holder);

        canvasAndHeightFactorList.add(new Pair<Canvas, Number>(canvas, heightFactor));
    }

}
