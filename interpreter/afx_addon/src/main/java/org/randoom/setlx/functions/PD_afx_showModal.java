package org.randoom.setlx.functions;

import javafx.application.Platform;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.afx.utilities.ui.ModalPane;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

/**
 * afx_showModal(title, message, buttonTexts, isError):
 *      adds a new button to the left panel of the AFX window.\\
 *      When it is clicked, the provided function is called asynchronously. During its execution, the button is disabled.
 *
 *      - title
 *          String representing the text to be displayed at the top, in larger font.
 *      - message
 *          String representing the text to be displayed in the middle.
 *      - buttonTexts
 *          List of Strings representing the desired buttons.\\
 *          Defaults to \texttt{["OK"]}.
 *      - isError
 *          Boolean indicating if the modal should be specially marked as an error.\\
 *          Defaults to \texttt{false}.
 *      <- an Integer specifying the index of the clicked button
 *
 * index := afx_showModal("Confirm Exit", "All changes will be lost!", ["abort", "OK"]);
 * if (index == 2) { exit; }
 */
public class PD_afx_showModal extends PreDefinedProcedure {
    private final static ParameterDefinition TITLE        = createParameter ("title");
    private final static ParameterDefinition MESSAGE      = createParameter ("message");
    private final static ParameterDefinition BUTTONTEXTS  = createOptionalParameter ("buttonTexts", Om.OM);
    private final static ParameterDefinition ISERROR      = createOptionalParameter ("isError", SetlBoolean.FALSE);
    public  final static PreDefinedProcedure DEFINITION   = new PD_afx_showModal();

    private PD_afx_showModal() {
        super();
        addParameter(TITLE);
        addParameter(MESSAGE);
        addParameter(BUTTONTEXTS);
        addParameter(ISERROR);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        //region parse arguments
        final Value titleValue = args.get(TITLE);
        if (!(titleValue instanceof SetlString)) {
            throw new IncompatibleTypeException("First argument '" + titleValue.toString(state) + "' is not a string.");
        }
        final String title = titleValue.getUnquotedString(state);

        final Value messageValue = args.get(MESSAGE);
        if (!(messageValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Second argument '" + messageValue.toString(state) + "' is not a string.");
        }
        final String message = messageValue.getUnquotedString(state);

        final Value buttonTextsValue = args.get(BUTTONTEXTS);
        final ArrayList<String> buttonTexts = new ArrayList<>();
        if (buttonTextsValue == Om.OM) {
            buttonTexts.add("OK");
        } else if (!(buttonTextsValue instanceof SetlList)) {
            throw new IncompatibleTypeException("Third argument '" + buttonTextsValue.toString(state) + "' is not a list.");
        } else {
            for (Value v : (SetlList)buttonTextsValue) {
                if (!(v instanceof SetlString)) {
                    throw new IncompatibleTypeException("Third argument '" + buttonTextsValue.toString(state) +
                            "' contains a value which is not a string.");
                }

                buttonTexts.add(v.getUnquotedString(state));
            }
        }

        final Value isErrorValue = args.get(ISERROR);
        if (!(isErrorValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Fourth argument '" + isErrorValue.toString(state) + "' is not a boolean.");
        }
        final boolean isError = isErrorValue.equalTo(SetlBoolean.TRUE);
        //endregion

        final ModalPane modalPane = AfxWindow.waitForStartUpCompleted().getModalPane();
        final CountDownLatch modalShownLatch = new CountDownLatch(1);

        Platform.runLater(new Runnable() {
              @Override
              public void run() {
                  if (isError) {
                      modalPane.showErrorModal(title, message, buttonTexts);
                  } else {
                      modalPane.showModal(title, message, buttonTexts);
                  }

                  modalShownLatch.countDown();
              }
        });

        try {
            modalShownLatch.await();
        } catch (InterruptedException e) {
            // ignore
        }
        int buttonIndex = modalPane.awaitButtonClick();
        return SetlDouble.valueOf(buttonIndex);
    }

}
