package org.randoom.setlx.afx.utilities;

import javafx.scene.canvas.GraphicsContext;

import java.util.Locale;

public class OutlinePositionHelper {
    public static OutlinePosition convert(String position) {
        position = position.toUpperCase(Locale.ENGLISH);

        switch (position) {
            case "MIDDLE":
                return OutlinePosition.MIDDLE;
            case "OUTSIDE":
                return OutlinePosition.OUTSIDE;
            case "INSIDE":
                return OutlinePosition.INSIDE;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static SizedBox calculate(
            OutlinePosition outlinePosition, GraphicsContext gc, double x, double y, double w, double h
    ) {
        final double strokeOffset = gc.getLineWidth() / 2;

        switch (outlinePosition) {
            case MIDDLE:
                return new SizedBox(x, y, w, h);
                // in JavaFX, the stroke is per default aligned in the middle
            case OUTSIDE:
                return new SizedBox(
                    x - strokeOffset, y - strokeOffset, w + 2*strokeOffset, h + 2*strokeOffset
                );
            case INSIDE:
                return new SizedBox(
                    x + strokeOffset, y + strokeOffset, w - 2*strokeOffset, h - 2*strokeOffset
                );
            default:
                throw new IllegalArgumentException();  // in practice, this should never happen
        }
    }

}
