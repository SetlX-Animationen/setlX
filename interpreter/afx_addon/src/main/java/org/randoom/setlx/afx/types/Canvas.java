package org.randoom.setlx.afx.types;

import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import org.randoom.setlx.afx.utilities.drawActions.DrawAction;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.CodeFragment;
import org.randoom.setlx.utilities.State;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A Wrapper Type for a JavaFX Canvas
 */
public class Canvas extends Value {
    private javafx.scene.canvas.Canvas javaFxCanvas;
    /**
     * Describes the relation of the width and height of the canvas.
     * I.e.: height = width * heightFactor
     */
    private double heightFactor;
    private boolean doCheckBounds = true;
    private ArrayList<DrawAction> currentDrawActions = new ArrayList<>();


    public Canvas(javafx.scene.canvas.Canvas javaFxCanvas, double heightFactor, boolean doCheckBounds) {
        this.javaFxCanvas  = javaFxCanvas;
        this.heightFactor  = heightFactor;
        this.doCheckBounds = doCheckBounds;
    }


    public double getHeightFactor() {
        return heightFactor;
    }

    public void setCheckBounds(boolean doCheckBounds) {
        this.doCheckBounds = doCheckBounds;
    }

    public boolean isCheckBounds() {
        return doCheckBounds;
    }


    /**
     * Adds a new DrawAction which will be used at the next call of `draw()`.
     * @param action The DrawAction to add.
     */
    public void addDrawAction(DrawAction action) {
        currentDrawActions.add(action);
    }

    /**
     * Clears the List of DrawActions which would be used at the next call of `draw()`.
     */
    public void clearDrawActions() {
        currentDrawActions.clear();
    }

    /**
     * Sets the planned DrawActions as current (overwriting the "old current" ones) and runs them.
     * The list of planned DrawActions is not altered, if new DrawActions are planed after this call, they are still
     *   added "on top".
     * @param state SetlX-State for writing output.
     */
    public void draw(final State state) {
        final GraphicsContext gc = javaFxCanvas.getGraphicsContext2D();

        Platform.runLater(new Runnable() {
            @Override public void run() {
                //region set default styling
                gc.setFill(Color.BLACK);

                gc.setStroke(Color.BLACK);
                gc.setLineWidth(1.0);

                gc.setFont(new Font("Arial", 17));
                gc.setTextAlign(TextAlignment.CENTER);
                //endregion

                // clear canvas:
                gc.clearRect(0, 0, getWidth(), getHeight());

                for (final DrawAction action : currentDrawActions) {
                    action.draw(getWidth(), getHeight(), gc);
                }
            }
        });
    }


    public double getWidth() {
        return javaFxCanvas.getWidth();
    }

    public double getHeight() {
        return javaFxCanvas.getHeight();
    }


    @Override
    public Value clone() {
        Canvas result = new Canvas(javaFxCanvas, heightFactor, doCheckBounds);

        for (DrawAction action : currentDrawActions) {
            result.addDrawAction(action.clone());
        }

        return result;
    }

    @Override
    public void appendString(State state, StringBuilder sb, int tabs) {
        sb.append("Canvas (with ").append(currentDrawActions.size()).append(" planned draw actions)");
    }

    @Override
    public int compareTo(CodeFragment other) {
        return (this.compareToOrdering() < other.compareToOrdering())? -1 : 1;
    }

    private final static long COMPARE_TO_ORDER_CONSTANT = generateCompareToOrderConstant(Canvas.class);

    @Override
    public long compareToOrdering() {
        return COMPARE_TO_ORDER_CONSTANT;
    }

    @Override
    public boolean equalTo(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Canvas)) {
            return false;
        }
        Canvas canvas = (Canvas)o;

        if (
            canvas.heightFactor != heightFactor
            || canvas.doCheckBounds != doCheckBounds
        ) {
            return false;
        }
        return Objects.equals(javaFxCanvas, canvas.javaFxCanvas);
    }

    @Override
    public int hashCode() {
        int result = (int)(heightFactor * 100);
        result += doCheckBounds ? 1000 : -1000;
        result += javaFxCanvas != null ? javaFxCanvas.hashCode() : 0;
        return result;
    }

}
