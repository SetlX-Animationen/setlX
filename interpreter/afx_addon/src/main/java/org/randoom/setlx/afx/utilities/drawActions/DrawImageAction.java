package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class DrawImageAction implements DrawAction {
    private Image image;
    /**
     * The x-part of the top left corner of the image's bounding box
     */
    private final double tlx;
    /**
     * The y-part of the top left corner of the image's bounding box
     */
    private final double tly;
    private final double width;
    private final double height;


    public DrawImageAction(
            Image image, double tlx, double tly, double width, double height
    ) {
        this.image = image;
        this.tlx = tlx;
        this.tly = tly;
        this.width = width;
        this.height = height;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        double cTlx = tlx*canvasWidth;
        double cTly = tly*canvasHeight;
        double cWidth = width*canvasWidth;
        double cHeight = height*canvasHeight;

        gc.drawImage(image, cTlx, cTly, cWidth, cHeight);
    }

    @Override
    public DrawAction clone() {
        return new DrawImageAction(image, tlx, tly, width, height);
    }

}
