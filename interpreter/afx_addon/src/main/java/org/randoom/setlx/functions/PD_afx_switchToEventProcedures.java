package org.randoom.setlx.functions;

import org.randoom.setlx.exceptions.ExitException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

/**
 * afx_switchToEventProcedures():
 *      switches to only using Event Procedures (i.e. from Button-Presses).\\
 *      This effectively halts the standard, procedural loop for the remaining runtime of the program.
 *
 *      <- \texttt{om}.
 *
 *  afx_switchToEventProcedures();
 */
public class PD_afx_switchToEventProcedures extends PreDefinedProcedure {
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_switchToEventProcedures();

    private PD_afx_switchToEventProcedures() {
        super();
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        CountDownLatch neverEndingLatch = new CountDownLatch(1);

        try {
            neverEndingLatch.await();
        } catch (InterruptedException e) {
            state.outWriteLn("Sleep of SetlX Thread-Loop interrupted! Exiting!");
            throw new ExitException();
        }

        return Om.OM;
    }

}
