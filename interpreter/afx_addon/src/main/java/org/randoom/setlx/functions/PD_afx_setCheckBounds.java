package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlBoolean;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_setCheckBounds(canvas, doCheckBounds):
 *      sets if Draw Calls to this Canvas should check if the boundaries resulting from the sizings passed to them are
 *      inside the Canvas.\\
 *      The default State is true, i.e. if sizings which would result in drawing out-of-bounds of the Canvas are passed
 *      to a Draw Call, it will abort with an error.
 *
 *      - canvas
 *          Canvas Reference for which teh Boundaries Checking should be set.
 *      - doCheckBounds
 *          Boolean, representing the desired State of the Boundaries Checking.\\
 *          \texttt{true} equals activated.
 *      <- \texttt{om}.
 *
 *  afx_setCheckBounds(canvas, false);
 */
public class PD_afx_setCheckBounds extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition DOCHECKBOUNDS = createParameter("doCheckBounds");
    public final static PreDefinedProcedure DEFINITION     = new PD_afx_setCheckBounds();

    private PD_afx_setCheckBounds() {
        super();
        addParameter(DOCHECKBOUNDS);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(State state, HashMap<ParameterDefinition, Value> args, Canvas canvas) throws SetlException {
        final Value doCheckBoundsValue = args.get(DOCHECKBOUNDS);
        if (!(doCheckBoundsValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Second argument '" + doCheckBoundsValue.toString(state) + "' is not a boolean.");
        }
        final boolean doCheckBounds = doCheckBoundsValue.equalTo(SetlBoolean.TRUE);

        canvas.setCheckBounds(doCheckBounds);

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
