package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.OutlinePosition;
import org.randoom.setlx.afx.utilities.OutlinePositionHelper;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.DrawEllipseAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planDrawEllipse(canvas, tlx, tly, width, height, doFill, doOutline, outlinePosition):
 *      plans to draw a Ellipse onto the given canvas, using Coordinates for the upper-left corner and sizes for
 *        the width and height at it's center.
 *
 *      - canvas
 *          Canvas Reference onto which the Ellipse should be drawn.
 *      - tlx / tly
 *          Number, representing the $x$ and $y$ Coordinate of the upper left corner of the enclosing area, into which
 *          the Ellipse will be drawn.
 *      - width
 *          Number, representing the width of the Ellipse at its center.
 *      - height
 *          Number, representing the height of the Ellipse at its center.
 *      - doFill
 *          Boolean, specifying if the drawn Ellipse should be filled.\\
 *          Defaults to \texttt{true}.
 *      - doOutline
 *          Boolean, specifying if the drawn Ellipse should be outlined.\\
 *          Defaults to \texttt{false}.
 *      - outlinePosition
 *          String, specifying where the outline-stroke should be placed, relative to the boundary of the Ellipse.\\
 *          Possible Values: \texttt{"MIDDLE"}, \texttt{"OUTSIDE"}, \texttt{"INSIDE"}.\\
 *          Defaults to \texttt{"MIDDLE"}.
 *      <- \texttt{om}.
 *
 *  afx_planDrawEllipse(canvas, 0.25, 0.25, 0.25, 0.125);
 *  afx_planDrawEllipse(canvas, 0.75, 0.75, 0.125, 0.25, false, true);
 *  afx_planDrawEllipse(canvas, 0.75, 0.75, 0.125, 0.25, false, true, "MIDDLE");
 */
public class PD_afx_planDrawEllipse extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition TLX         = createParameter ("tlx");
    private final static ParameterDefinition TLY         = createParameter ("tly");
    private final static ParameterDefinition WIDTH       = createParameter ("width");
    private final static ParameterDefinition HEIGHT      = createParameter ("height");
    private final static ParameterDefinition DOFILL      = createOptionalParameter ("doFill", SetlBoolean.TRUE);
    private final static ParameterDefinition DOOUTLINE   = createOptionalParameter ("doOutline", SetlBoolean.FALSE);
    private final static ParameterDefinition OUTLINEPOS  = createOptionalParameter ("outlinePosition", new SetlString("MIDDLE"));
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planDrawEllipse();

    private PD_afx_planDrawEllipse() {
        super();
        addParameter(TLX);
        addParameter(TLY);
        addParameter(WIDTH);
        addParameter(HEIGHT);
        addParameter(DOFILL);
        addParameter(DOOUTLINE);
        addParameter(OUTLINEPOS);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        //region parse arguments
        final Value tlxValue = args.get(TLX);
        if (!(tlxValue instanceof SetlDouble || tlxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not a number.");
        }
        final double tlx = tlxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tlx >= 0 && tlx <= 1)) {
            throw new IncompatibleTypeException("Second argument '" + tlxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value tlyValue = args.get(TLY);
        if (!(tlyValue instanceof SetlDouble || tlyValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not a number.");
        }
        final double tly = tlyValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tly >= 0 && tly <= 1)) {
            throw new IncompatibleTypeException("Third argument '" + tlyValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value widthValue = args.get(WIDTH);
        if (!(widthValue instanceof SetlDouble || widthValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + widthValue.toString(state) + "' is not a number.");
        }
        final double width = widthValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(width >= 0 && width <= 1)) {
            throw new IncompatibleTypeException("Fourth argument '" + widthValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value heightValue = args.get(HEIGHT);
        if (!(heightValue instanceof SetlDouble || heightValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fifth argument '" + heightValue.toString(state) + "' is not a number.");
        }
        final double height = heightValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(height >= 0 && height <= 1)) {
            throw new IncompatibleTypeException("Fifth argument '" + heightValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value doFillValue = args.get(DOFILL);
        if (!(doFillValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Sixth argument '" + doFillValue.toString(state) + "' is not a boolean.");
        }
        final boolean doFill = doFillValue.equalTo(SetlBoolean.TRUE);

        final Value doOutlineValue = args.get(DOOUTLINE);
        if (!(doOutlineValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException("Seventh argument '" + doOutlineValue.toString(state) + "' is not a boolean.");
        }
        final boolean doOutline = doOutlineValue.equalTo(SetlBoolean.TRUE);

        final Value outlinePosValue = args.get(OUTLINEPOS);
        if (!(outlinePosValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Eighth argument '" + outlinePosValue.toString(state) + "' is not a string.");
        }
        final OutlinePosition outlinePosition;
        try {
            outlinePosition = OutlinePositionHelper.convert(outlinePosValue.getUnquotedString(state));
        } catch (IllegalArgumentException ex) {
            throw new IncompatibleTypeException("Eighth argument '" + outlinePosValue.toString(state) + "' is not valid.");
        }
        //endregion

        if (canvas.isCheckBounds() && !(tlx + width <= 1 && tly + height <= 1)) {
            throw new IncompatibleTypeException("Provided Sizings would be out-of-bounds.");
        }

        canvas.addDrawAction(new DrawEllipseAction(tlx, tly, width, height, doFill, doOutline, outlinePosition));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
