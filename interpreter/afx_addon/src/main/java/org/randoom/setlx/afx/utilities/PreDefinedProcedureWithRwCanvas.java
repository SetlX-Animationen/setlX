package org.randoom.setlx.afx.utilities;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.functions.PreDefinedProcedure;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * Base for Pre-Defined Procedures which are taking a afx.types.Canvas as their first, read-write argument.
 */
public abstract class PreDefinedProcedureWithRwCanvas extends PreDefinedProcedure {
    protected final static ParameterDefinition CANVAS = createRwParameter("canvas");

    protected PreDefinedProcedureWithRwCanvas() {
        super();
        addParameter(CANVAS);
    }

    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value canvasValue = args.get(CANVAS);
        if (!(canvasValue instanceof Canvas)) {
            throw new IncompatibleTypeException("First argument '" + canvasValue.toString(state) + "' is not a canvas.");
        }
        final Canvas canvas = (Canvas)canvasValue;

        Pair<Canvas, Value> result = executeFurther(state, args, canvas);
        args.put(CANVAS, result.a);

        return result.b;
    }

    /**
     * To be overridden with the implementation of the further execution after the Canvas argument was successfully
     *  extracted.
     * @param state The SetlX-State
     * @param args The original argument-HashMap
     * @param canvas The extracted Canvas object
     * @return New State of the Canvas and the desired return value of the Procedure.
     */
    public abstract Pair<Canvas, Value> executeFurther(
            final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas
        ) throws SetlException
    ;

}
