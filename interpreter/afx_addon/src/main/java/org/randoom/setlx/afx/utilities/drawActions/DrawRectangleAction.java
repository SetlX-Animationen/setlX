package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;
import org.randoom.setlx.afx.utilities.OutlinePosition;
import org.randoom.setlx.afx.utilities.OutlinePositionHelper;
import org.randoom.setlx.afx.utilities.SizedBox;

public class DrawRectangleAction implements DrawAction {
    /**
     * The x-part of the top left corner of the rectangle's bounding box
     */
    private final double tlx;
    /**
     * The y-part of the top left corner of the rectangle's bounding box
     */
    private final double tly;
    private final double width;
    private final double height;
    private final boolean doFill;
    private final boolean doOutline;
    private final OutlinePosition outlinePosition;


    public DrawRectangleAction(
            double tlx, double tly, double width, double height,
            boolean doFill, boolean doOutline, OutlinePosition outlinePosition
    ) {
        this.tlx = tlx;
        this.tly = tly;
        this.width = width;
        this.height = height;
        this.doFill = doFill;
        this.doOutline = doOutline;
        this.outlinePosition = outlinePosition;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        double cTlx = tlx*canvasWidth;
        double cTly = tly*canvasHeight;
        double cWidth = width*canvasWidth;
        double cHeight = height*canvasHeight;

        if (doFill) {
            gc.fillRect(cTlx, cTly, cWidth, cHeight);
        }

        if (doOutline) {
            final SizedBox strokeBox = OutlinePositionHelper.calculate(outlinePosition, gc, cTlx, cTly, cWidth, cHeight);
            gc.strokeRect(strokeBox.x, strokeBox.y, strokeBox.w, strokeBox.h);
        }
    }

    @Override
    public DrawAction clone() {
        return new DrawRectangleAction(tlx, tly, width, height, doFill, doOutline, outlinePosition);
    }

}
