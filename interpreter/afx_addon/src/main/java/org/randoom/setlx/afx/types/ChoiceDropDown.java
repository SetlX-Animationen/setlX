package org.randoom.setlx.afx.types;

import javafx.application.Platform;
import javafx.scene.control.ChoiceBox;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.CodeFragment;
import org.randoom.setlx.utilities.State;

import java.util.Objects;

/**
 * A Wrapper Type for a JavaFX ChoiceBox
 */
public class ChoiceDropDown extends Input {
    private ChoiceBox<String> javaFxControl;

    public ChoiceDropDown(ChoiceBox<String> javaFxControl){
        this.javaFxControl = javaFxControl;
    }


    @Override
    public ChoiceBox<String> getControl(){
        return javaFxControl;
    }

    @Override
    public Value getValue() {
        return new SetlString(javaFxControl.getValue());
    }

    @Override
    public void clearValue() {
        Platform.runLater(new Runnable() {
            @Override public void run() {
                javaFxControl.setValue(javaFxControl.getItems().get(0));
            }
        });
    }


    @Override
    public Value clone() {
        return new ChoiceDropDown(javaFxControl);
    }

    @Override
    public void appendString(State state, StringBuilder sb, int tabs) {
        sb.append("ChoiceDropDown");
    }

    @Override
    public int compareTo(CodeFragment other) {
        return (this.compareToOrdering() < other.compareToOrdering())? -1 : 1;
    }

    private final static long COMPARE_TO_ORDER_CONSTANT = generateCompareToOrderConstant(ChoiceDropDown.class);

    @Override
    public long compareToOrdering() {
        return COMPARE_TO_ORDER_CONSTANT;
    }

    @Override
    public boolean equalTo(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ChoiceDropDown input = (ChoiceDropDown)o;
        return Objects.equals(javaFxControl, input.javaFxControl);
    }

    @Override
    public int hashCode() {
        return javaFxControl != null ? javaFxControl.hashCode() : 0;
    }

}
