package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;

public class SetTextFontAction implements DrawAction {
    private Font font;


    public SetTextFontAction(Font font) {
        this.font = font;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        gc.setFont(font);
    }

    @Override
    public DrawAction clone() {
        return new SetTextFontAction(font);
    }

}
