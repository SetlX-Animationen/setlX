package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_getCanvasWidth(canvas):
 *      determines the current width of the given canvas.
 *
 *      - canvas
 *          Canvas Reference specifying which planned draw actions should be executed.
 *      <- a \texttt{double} equal to the current width of the given canvas in pixels.
 *
 *  afx_getCanvasWidth(canvas);
 */
public class PD_afx_getCanvasWidth extends PreDefinedProcedureWithRwCanvas {
    public final static PreDefinedProcedure DEFINITION           = new PD_afx_getCanvasWidth();

    private PD_afx_getCanvasWidth() {
        super();
    }

    @Override
    public Pair<Canvas, Value> executeFurther(State state, HashMap<ParameterDefinition, Value> args, Canvas canvas) throws SetlException {
        return new Pair<>(canvas, (Value)SetlDouble.valueOf(canvas.getWidth()));
    }

}
