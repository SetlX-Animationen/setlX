package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.afx.utilities.ui.ControlsPane;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.operatorUtilities.OperatorExpression;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.FragmentList;
import org.randoom.setlx.utilities.State;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * afx_addButton(text, callArgs, procedure, keyBind):
 *      adds a new button to the left panel of the AFX window.\\
 *      When it is clicked, the provided function is called asynchronously. During its execution, the button is disabled.
 *
 *      - text
 *          String representing the label to be displayed on the button.
 *      - callArgs
 *          List containing Values of arbitrary type which will be passed to the provided function as arguments when it
 *          is called due to the button.\\
 *          If it equals \texttt{om} or an empty list, the provided function will be called with no arguments.
 *      - procedure
 *          Procedure which is called once the button is clicked.
 *      - keyBind
 *          String specifying a keyboard key which will also call the procedure when pressed.\\
 *          For the list of possible values, consult \href{
 *              https://docs.oracle.com/javafx/2/api/javafx/scene/input/KeyCode.html#enum_constant_summary
 *          }{the JavaFX documentation}. As a convenience, a String containing a single digit will be converted to key
 *          binds representing the digit both in the numeric row and the num-pad.\\
 *          Defaults to binding no keyboard key to the button.
 *      <- a Handle to the newly created Button.
 *
 * draw := procedure(widthInput, canvas) {
 *     width := double(afx_getInputValue(widthInput));
 *     afx_planDrawTextCentered(canvas, "Hello World!", width, width);
 *     // etc.
 * }
 * // ... necessary Initialization redacted
 * afx_addButton("Go!", [input1, topCanvas], draw);
 * afx_addButton("Go!", [input2, bottomCanvas], draw, "ENTER");
 */
public class PD_afx_addButton extends PreDefinedProcedure {
    private final static ParameterDefinition TEXT        = createParameter ("text");
    private final static ParameterDefinition CALLARGS    = createParameter ("callArgs");
    private final static ParameterDefinition PROCEDURE   = createParameter ("procedure");
    private final static ParameterDefinition KEYBIND     = createOptionalParameter ("keyBind", Om.OM);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_addButton();

    private PD_afx_addButton() {
        super();
        addParameter(TEXT);
        addParameter(CALLARGS);
        addParameter(PROCEDURE);
        addParameter(KEYBIND);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        //region parse arguments
        final Value textValue = args.get(TEXT);
        if (!(textValue instanceof SetlString)) {
            throw new IncompatibleTypeException("First argument '" + textValue.toString(state) + "' is not a string.");
        }
        final String text = textValue.getUnquotedString(state);

        final Value callArgsValue = args.get(CALLARGS);
        final List<Value> argsList = new ArrayList<Value>();
        if (callArgsValue != Om.OM) {
            if (!(callArgsValue instanceof SetlList)) {
                throw new IncompatibleTypeException("Second argument '" + callArgsValue.toString(state) + "' is not a List.");
            }

            for (Value v : (SetlList)callArgsValue) {
                argsList.add(v);
            }
        }

        final Value procedureValue = args.get(PROCEDURE);
        if (!(procedureValue instanceof Procedure)) {
            throw new IncompatibleTypeException("Third argument '" + procedureValue.toString(state) + "' is not a procedure.");
        }
        final Procedure procedure = (Procedure)procedureValue;

        final Value keyBindValue = args.get(KEYBIND);
        final ArrayList<KeyCode> keyBinds = new ArrayList<>();
        if (keyBindValue != Om.OM) {
            if (!(keyBindValue instanceof SetlString)) {
                throw new IncompatibleTypeException("Fourth argument '" + keyBindValue.toString(state) + "' is not a string.");
            }

            final String keyText = keyBindValue.getUnquotedString(state).toUpperCase(Locale.ENGLISH);
            if (keyText.matches("\\d")) {
                keyBinds.add(KeyCode.valueOf("DIGIT" + keyText));
                keyBinds.add(KeyCode.valueOf("NUMPAD" + keyText));
            } else {
                try {
                    keyBinds.add(KeyCode.valueOf(keyText));
                } catch (IllegalArgumentException ex) {
                    throw new IncompatibleTypeException("Fourth argument '" + keyBindValue.toString(state) + "' is not valid.");
                }
            }
        }
        //endregion

        final AfxWindow afxWindow = AfxWindow.waitForStartUpCompleted();

        final javafx.scene.control.Button javaFxButton = new javafx.scene.control.Button(text);
        javaFxButton.setOnAction(new EventHandler<ActionEvent>() {
            // at this point we were called from the JavaFX Thread and are therefore in it
            @Override public void handle(ActionEvent actionEvent) {
                try {
                    afxWindow.inputBlockedSemaphore.acquire();
                } catch (InterruptedException e) {
                    // ignore
                    return;
                }

                final ControlsPane controlsPane = afxWindow.getControlsPane();
                controlsPane.setBlocked(true);    // to avoid calling the procedure again while it is executed

                Thread processingThread = new Thread(){
                    // we need to switch to a new thread so the JavaFX thread isn't blocked and changes to canvases can
                    //  therefore be executed
                    public void run(){
                        try {
                            procedure.call(state, argsList, new FragmentList<OperatorExpression>(), null, null);
                        }
                        catch (SetlException ex) {
                            state.outWriteLn("Error while trying to execute the given function for your Button! Details:");
                            ex.printExceptionsTraceAndReplay(state);
                        }

                        Platform.runLater(new Runnable() {
                            // switch back to the JavaFX Thread to be able to modify the window's state directly
                            @Override public void run() {
                                controlsPane.setBlocked(false);
                                afxWindow.inputBlockedSemaphore.release();
                            }
                        });
                    }
                };

                processingThread.start();
            }
        });

        Platform.runLater(new Runnable() {
            @Override public void run() {
                afxWindow.getControlsPane().addNode(javaFxButton);
            }
        });
        for (KeyCode kc : keyBinds) {
            afxWindow.addGlobalKeyBind(kc, javaFxButton);
        }

        return new org.randoom.setlx.afx.types.Button(javaFxButton);
    }

}
