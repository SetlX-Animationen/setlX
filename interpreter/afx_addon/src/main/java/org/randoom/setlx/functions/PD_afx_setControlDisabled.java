package org.randoom.setlx.functions;

import javafx.application.Platform;
import org.randoom.setlx.afx.types.Control;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlBoolean;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_setControlDisabled(control, isDisabled):
 *      sets if the given Control is disabled. Note that Inputs are a subclass of Controls.\\
 *      A disabled Controls renders differently to convey that fact and does not allow interaction by the user with it.
 *
 *      - control
 *          Reference to the Control which should be modified.
 *      - isDisabled
 *          Boolean representing if the Control should be disabled.\\
 *          Defaults to \texttt{true}, i.e. a disabled Control.
 *      <- \texttt{om}.
 *
 * afx_setInputDisabled(input);
 * afx_setInputDisabled(input, false);
 */
public class PD_afx_setControlDisabled extends PreDefinedProcedure {
    protected final static ParameterDefinition CONTROL     = createParameter("control");
    protected final static ParameterDefinition ISDISABLED  = createOptionalParameter("isDisabled", SetlBoolean.TRUE);
    public final static PreDefinedProcedure DEFINITION     = new PD_afx_setControlDisabled();

    private PD_afx_setControlDisabled() {
        super();
        addParameter(CONTROL);
        addParameter(ISDISABLED);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value controlValue = args.get(CONTROL);
        if (!(controlValue instanceof Control)) {
            throw new IncompatibleTypeException("First argument '" + controlValue.toString(state) + "' is not a Control.");
        }
        final Control control = (Control)controlValue;

        final Value isDisabledValue = args.get(ISDISABLED);
        if (!(isDisabledValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException(
                    "Second argument '" + isDisabledValue.toString(state) + "' is not a boolean."
            );
        }
        final boolean isDisabled = isDisabledValue.equalTo(SetlBoolean.TRUE);

        Platform.runLater(new Runnable() {
            @Override public void run() {
                control.setDisable(isDisabled);
            }
        });

        return Om.OM;
    }

}
