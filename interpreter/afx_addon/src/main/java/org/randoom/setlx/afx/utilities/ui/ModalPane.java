package org.randoom.setlx.afx.utilities.ui;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ModalPane implements Pane {
    private final double modalWidth = 400;

    private StackPane root;
    private Label title;
    private Label message;
    private HBox buttonBox;
    private StackPane errorBackground;
    private FadeTransition blinkErrorTransition;

    private CountDownLatch buttonClickedLatch;
    private boolean didBlockAfxWindow;

    private int selectedButtonIndex;


    ModalPane() {
        root = new StackPane();
        root.setVisible(false);

        StackPane background = new StackPane();
        background.setStyle(StyleConstants.darkGrayBackgroundStyle);
        background.setOpacity(0.75);
        root.getChildren().add(background);

        HBox modalPositioner = new HBox();
        modalPositioner.setFillHeight(false);
        modalPositioner.setAlignment(Pos.CENTER);
        root.getChildren().add(modalPositioner);

        StackPane modal = new StackPane();
        modal.setPrefWidth(modalWidth);
        modal.setMinWidth(modalWidth);
        modal.setMaxWidth(modalWidth);
        modal.setStyle(StyleConstants.darkGrayBackgroundStyle + StyleConstants.roundedCornersStyle);
        modalPositioner.getChildren().add(modal);

        errorBackground = new StackPane();
        errorBackground.setStyle(StyleConstants.errorRedBackgroundStyle + StyleConstants.roundedCornersStyle);
        blinkErrorTransition = new FadeTransition(Duration.millis(1000), errorBackground);
        blinkErrorTransition.setFromValue(1.0);
        blinkErrorTransition.setToValue(0.025);
        blinkErrorTransition.setCycleCount(1);
        errorBackground.setVisible(false);
        modal.getChildren().add(errorBackground);

        VBox modalContents = new VBox();
        modalContents.setPadding(StyleConstants.onAllSidesSpacingInset);
        modalContents.setSpacing(StyleConstants.spacingSize);
        modal.getChildren().add(modalContents);

        title = new Label();
        title.setFont(new Font(21));
        title.setTextFill(Color.LIGHTGRAY);
        title.setWrapText(true);
        title.setTextAlignment(TextAlignment.JUSTIFY);
        modalContents.getChildren().add(title);

        message = new Label();
        message.setTextFill(Color.LIGHTGRAY);
        message.setWrapText(true);
        message.setTextAlignment(TextAlignment.JUSTIFY);
        modalContents.getChildren().add(message);

        buttonBox = new HBox();
        buttonBox.setAlignment(Pos.CENTER_RIGHT);
        buttonBox.setSpacing(StyleConstants.spacingSize);
        buttonBox.setPadding(new Insets(StyleConstants.spacingSize, 0, 0, 0));
        // to separate the buttons more distinctly from the texts
        modalContents.getChildren().add(buttonBox);
    }


    public void showModal(String titleText, String messageText, List<String> buttonTexts) {
        title.setText(titleText);
        message.setText(messageText);

        buttonBox.getChildren().clear();
        for (int i = 0; i < buttonTexts.size(); i++) {
            Button b = new Button(buttonTexts.get(i));
            final int finalI = i;
            b.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    selectedButtonIndex = finalI;
                    buttonClickedLatch.countDown();
                }
            });
            if (i == buttonTexts.size() - 1) {
                b.setDefaultButton(true);
            }
            buttonBox.getChildren().add(b);
        }

        selectedButtonIndex = buttonTexts.size() - 1;  // good enough default for errors
        buttonClickedLatch = new CountDownLatch(1);

        root.setVisible(true);

        didBlockAfxWindow = AfxWindow.waitForStartUpCompleted().inputBlockedSemaphore.tryAcquire();
        // if the window was already blocked by a button press / key bind, we don't want to wait until that's over
    }

    public void showErrorModal(String titleText, String messageText, List<String> buttonTexts) {
        errorBackground.setVisible(true);
        blinkErrorTransition.jumpTo(Duration.millis(0));
        blinkErrorTransition.play();

        showModal(titleText, messageText, buttonTexts);
    }

    public int awaitButtonClick() {
        try {
            buttonClickedLatch.await();
        } catch (InterruptedException e) {
            // ignore
        }

        root.setVisible(false);
        errorBackground.setVisible(false);

        if (didBlockAfxWindow) {
            AfxWindow.waitForStartUpCompleted().inputBlockedSemaphore.release();
        }

        return selectedButtonIndex;
    }


    @Override
    public Node getNode() {
        return root;
    }

    @Override
    public double getWidth() {
        return root.getWidth();
    }

    @Override
    public double getHeight() {
        return root.getHeight();
    }

}
