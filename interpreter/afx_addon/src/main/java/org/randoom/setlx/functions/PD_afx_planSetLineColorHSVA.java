package org.randoom.setlx.functions;

import javafx.scene.paint.Color;
import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.ColorConverter;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.SetBorderColorAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Rational;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planSetLineColorHSVA(canvas, hue, saturation, value, alpha):
 *      plans to change the Line Color to the one defined by the given HSV+A values.\\
 *      The corresponding \href{https://en.wikibooks.org/wiki/Color_Models:_RGB,_HSV,_HSL#HSV}{HSV Color Model} is
 *      visualized in Figure \ref{fig:hsv-color-model} on page \pageref{fig:hsv-color-model}.
 *
 *      - canvas
 *          Canvas Reference for which the Line Color should be set.
 *      - hue
 *          Number representing the desired Hue in degrees.
 *      - saturation
 *          Number representing the desired Saturation. Its valid range is defined as follows: $0.0 \leq s \leq 1.0$
 *      - value
 *          Number representing the desired Value. Its valid range is defined as follows: $0.0 \leq v \leq 1.0$
 *      - alpha
 *          Number representing the desired Alpha. A value of $1.0$ represents full opacity, $0.0$ represents
 *          full transparency. Its valid range is defined as follows: $0.0 \leq a \leq 1.0$\\
 *          Defaults to \texttt{1.0}.
 *      <- \texttt{om}.
 *
 * afx_planSetLineColorHSVA(canvas, 120, 0.9, 0.9);
 * afx_planSetLineColorHSVA(canvas, 120, 0.9, 0.9, 0.7);
 */
public class PD_afx_planSetLineColorHSVA extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition HUE         = createParameter("hue");
    private final static ParameterDefinition SATURATION  = createParameter("saturation");
    private final static ParameterDefinition VALUE       = createParameter("value");
    private final static ParameterDefinition ALPHA       = createOptionalParameter ("alpha", SetlDouble.ONE);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planSetLineColorHSVA();

    private PD_afx_planSetLineColorHSVA() {
        super();
        addParameter(HUE);
        addParameter(SATURATION);
        addParameter(VALUE);
        addParameter(ALPHA);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        final Value hueValue = args.get(HUE);
        if (!(hueValue instanceof SetlDouble || hueValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + hueValue.toString(state) + "' is not a number.");
        }
        final double hue = hueValue.toJDoubleValue(state);

        final Value saturationValue = args.get(SATURATION);
        if (!(saturationValue instanceof SetlDouble || saturationValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + saturationValue.toString(state) + "' is not a number.");
        }
        final double saturation = saturationValue.toJDoubleValue(state);

        final Value valueValue = args.get(VALUE);
        if (!(valueValue instanceof SetlDouble || valueValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + valueValue.toString(state) + "' is not a number.");
        }
        final double value = valueValue.toJDoubleValue(state);

        final Value alphaValue = args.get(ALPHA);
        if (!(alphaValue instanceof SetlDouble || alphaValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fifth argument '" + alphaValue.toString(state) + "' is not a number.");
        }
        final double alpha = alphaValue.toJDoubleValue(state);

        Color color;
        try {
            color = ColorConverter.hsvaToJavaFxColor(hue, saturation, value, alpha);
        } catch (IllegalArgumentException ex) {
            throw new IncompatibleTypeException("The given values don't form a supported color.");
        }
        canvas.addDrawAction(new SetBorderColorAction(color));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
