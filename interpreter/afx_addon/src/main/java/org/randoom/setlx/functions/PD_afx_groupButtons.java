package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.scene.layout.FlowPane;
import org.randoom.setlx.afx.types.Button;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.afx.utilities.ui.ControlsPane;
import org.randoom.setlx.afx.utilities.ui.StyleConstants;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlList;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * afx_groupButtons(buttonList):
 *      groups Buttons visually together by taking them out of the normal, vertical flow and putting them (as much as
 *      possible) horizontally besides each other.
 *
 *      - buttonList
 *          List of References to the Buttons which should be grouped together
 *      <- \texttt{om}.
 *
 * btn1 := afx_addButton("insert", om, insert);
 * btn2 := afx_addButton("delete", om, delete);
 * afx_groupButtons([btn1, btn2]);
 */
public class PD_afx_groupButtons extends PreDefinedProcedure {
    protected final static ParameterDefinition BUTTONLIST  = createParameter("buttonList");
    public final static PreDefinedProcedure DEFINITION     = new PD_afx_groupButtons();

    private PD_afx_groupButtons() {
        super();
        addParameter(BUTTONLIST);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value buttonListValue = args.get(BUTTONLIST);
        final ArrayList<Button> buttonList = new ArrayList<>();
        if (!(buttonListValue instanceof SetlList)) {
            throw new IncompatibleTypeException("First argument '" + buttonListValue.toString(state) + "' is not a list.");
        } else {
            for (Value v : (SetlList)buttonListValue) {
                if (!(v instanceof Button)) {
                    throw new IncompatibleTypeException("First argument '" + buttonListValue.toString(state) +
                            "' contains a value which is not a button.");
                }

                buttonList.add((Button)v);
            }
        }

        Platform.runLater(new Runnable() {
            @Override public void run() {
                ControlsPane controlPane = AfxWindow.waitForStartUpCompleted().getControlsPane();

                FlowPane flowPane = new FlowPane();
                flowPane.setHgap(StyleConstants.spacingSize);

                for (Button btn : buttonList) {
                    controlPane.removeNode(btn.getControl());
                    flowPane.getChildren().add(btn.getControl());
                }

                controlPane.addNode(flowPane);
            }
        });

        return Om.OM;
    }

}
