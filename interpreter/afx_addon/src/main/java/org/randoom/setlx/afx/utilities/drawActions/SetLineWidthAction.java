package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;

public class SetLineWidthAction implements DrawAction {
    private double width;


    public SetLineWidthAction(double size) {
        this.width = size;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        gc.setLineWidth(width);
    }

    @Override
    public DrawAction clone() {
        return new SetLineWidthAction(width);
    }

}
