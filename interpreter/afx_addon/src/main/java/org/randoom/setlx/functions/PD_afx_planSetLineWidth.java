package org.randoom.setlx.functions;

import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.SetLineWidthAction;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Rational;
import org.randoom.setlx.types.SetlDouble;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_planSetLineWidth(canvas, width):
 *      plans to set the width of Lines, used for borders and stroke lines, to the given width.\\
 *
 *      - canvas
 *          Canvas Reference for which the Line width should be set.
 *      - width
 *          Number representing the desired Line width in pixels. It's value needs to be bigger then 0.
 *      <- \texttt{om}.
 *
 *  afx_planSetLineWidth(canvas, 10);
 */
public class PD_afx_planSetLineWidth extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition WIDTH     = createParameter("width");
    public final static PreDefinedProcedure DEFINITION = new PD_afx_planSetLineWidth();

    private PD_afx_planSetLineWidth() {
        super();
        addParameter(WIDTH);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(State state, HashMap<ParameterDefinition, Value> args, Canvas canvas) throws SetlException {
        final Value widthValue = args.get(WIDTH);
        if (!(widthValue instanceof SetlDouble || widthValue instanceof Rational)) {
            throw new IncompatibleTypeException("Second argument '" + widthValue.toString(state) + "' is not a number.");
        }
        final int width = widthValue.toJIntValue(state);
        if (width <= 0) {
            throw new IncompatibleTypeException("Second argument '" + width + "' needs to be bigger then 0.");
        }

        canvas.addDrawAction(new SetLineWidthAction(width));

        return new Pair<>(canvas, (Value)Om.OM);
    }

}
