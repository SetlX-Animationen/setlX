package org.randoom.setlx.functions;

import org.randoom.setlx.afx.types.Input;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_clearInputValue(input):
 *      clears the value of the given Input.
 *
 *      - input
 *          Reference to the Input Element whose value should be cleared.
 *      <- \texttt{om}.
 *
 * value := afx_getInputValue(input);
 * afx_clearInputValue(input);
 */
public class PD_afx_clearInputValue extends PreDefinedProcedure {
    protected final static ParameterDefinition INPUT    = createParameter("input");
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_clearInputValue();

    private PD_afx_clearInputValue() {
        super();
        addParameter(INPUT);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value inputValue = args.get(INPUT);
        if (!(inputValue instanceof Input)) {
            throw new IncompatibleTypeException("First argument '" + inputValue.toString(state) + "' is not an Input.");
        }
        final Input input = (Input)inputValue;

        input.clearValue();

        return Om.OM;
    }

}
