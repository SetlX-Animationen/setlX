package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.scene.control.CheckBox;
import org.randoom.setlx.afx.types.Checkbox;
import org.randoom.setlx.afx.utilities.ui.AfxWindow;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_addCheckbox(caption):
 *      adds a new Checkbox to the left pane of the AFX window.
 *
 *      - caption
 *          String representing the descriptive caption which should be displayed besides the box.
 *      <- a Handle to the newly created Input which can be used to read out its state.
 *
 *  afx_addCheckbox("display Nil states");
 */
public class PD_afx_addCheckbox extends PreDefinedProcedure {
    private final static ParameterDefinition CAPTION   = createParameter("caption");
    public final static PreDefinedProcedure DEFINITION = new PD_afx_addCheckbox();

    private PD_afx_addCheckbox() {
        super();
        addParameter(CAPTION);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value captionValue = args.get(CAPTION);
        if (!(captionValue instanceof SetlString)) {
            throw new IncompatibleTypeException("First argument '" + captionValue.toString(state) + "' is not a String.");
        }
        final String caption = captionValue.getUnquotedString(state);

        final CheckBox javaFxField = new CheckBox(caption);

        Platform.runLater(new Runnable() {
            @Override public void run() {
                AfxWindow.waitForStartUpCompleted().getControlsPane().addNode(javaFxField);
            }
        });

        return new Checkbox(javaFxField);
    }

}
