package org.randoom.setlx.functions;

import javafx.application.Platform;
import org.randoom.setlx.afx.types.Input;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_focusInput(input):
 *      sets the focus to the given Input. This allows the user to i.e. type directly into it, without needing to
 *      manually focus it through clicking on it first.
 *
 *      - input
 *          Reference to the Input Element which should be focused.
 *      <- \texttt{om}.
 *
 * afx_focusInput(input);
 */
public class PD_afx_focusInput extends PreDefinedProcedure {
    protected final static ParameterDefinition INPUT    = createParameter("input");
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_focusInput();

    private PD_afx_focusInput() {
        super();
        addParameter(INPUT);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value inputValue = args.get(INPUT);
        if (!(inputValue instanceof Input)) {
            throw new IncompatibleTypeException("First argument '" + inputValue.toString(state) + "' is not an Input.");
        }
        final Input input = (Input)inputValue;

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                input.getControl().requestFocus();
            }
        });

        return Om.OM;
    }

}
