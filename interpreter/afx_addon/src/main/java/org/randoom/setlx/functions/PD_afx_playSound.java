package org.randoom.setlx.functions;

import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Om;
import org.randoom.setlx.types.SetlBoolean;
import org.randoom.setlx.types.SetlString;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

/**
 * afx_playSound(fileName, doBlock):
 *      plays a Sound.
 *
 *      - fileName
 *          String representing the file name of the Sound to play, relative to the current working directory.
 *      - doBlock
 *          Boolean specifying if the execution of the remaining program should be blocked until the sound is finished
 *          playing.\\
 *          Defaults to \texttt{false}, i.e. the remaining code is immediately executed while the sound is being played
 *          asynchronously.
 *      <- \texttt{om}.
 *
 *  afx_playSound("Islaughterthemandfry.mp3");
 *  afx_playSound("Islaughterthemandfry.mp3", true);
 */
public class PD_afx_playSound extends PreDefinedProcedure {
    private final static ParameterDefinition FILE_NAME   = createParameter ("fileName");
    private final static ParameterDefinition DO_BLOCK    = createOptionalParameter ("doBlock", SetlBoolean.FALSE);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_playSound();

    private PD_afx_playSound() {
        super();
        addParameter(FILE_NAME);
        addParameter(DO_BLOCK);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args)
            throws SetlException
    {
        final Value fileNameValue = args.get(FILE_NAME);
        if (!(fileNameValue instanceof SetlString)) {
            throw new IncompatibleTypeException(
                "First argument '" + fileNameValue.toString(state) + "' is not a string."
            );
        }
        final String fileName = state.filterFileName(fileNameValue.getUnquotedString(state));

        final Value doBlockValue = args.get(DO_BLOCK);
        if (!(doBlockValue instanceof SetlBoolean)) {
            throw new IncompatibleTypeException(
                    "Second argument '" + doBlockValue.toString(state) + "' is not a boolean."
            );
        }
        final boolean doBlock = doBlockValue.equalTo(SetlBoolean.TRUE);

        final Media sound = new Media(new File(fileName).toURI().toString());

        final CountDownLatch latch = new CountDownLatch(doBlock ? 1 : 0);

        Platform.runLater(new Runnable() {
            @Override public void run() {
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setOnEndOfMedia(new Runnable() {
                    @Override
                    public void run() {
                        latch.countDown();
                    }
                });
                mediaPlayer.play();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            // ignore
        }
        return Om.OM;
    }

}

