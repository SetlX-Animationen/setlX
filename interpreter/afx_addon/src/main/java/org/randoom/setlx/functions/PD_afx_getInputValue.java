package org.randoom.setlx.functions;

import org.randoom.setlx.afx.types.Input;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.State;

import java.util.HashMap;

/**
 * afx_getInputValue(input):
 *      reads the current value of the given Input.
 *
 *      - input
 *          Reference to the Input Element whose value should be read.
 *      <- The Value representing the state currently set by the user for the given control.\\
 *          The specific return type is depended on which Input Element was passed. I.e. a Checkbox returns a Boolean,
 *          a TextInput returns a String.
 *
 * input := afx_addTextInput();
 * value := afx_getInputValue(input);
 */
public class PD_afx_getInputValue extends PreDefinedProcedure {
    protected final static ParameterDefinition INPUT    = createParameter("input");
    public final static PreDefinedProcedure DEFINITION  = new PD_afx_getInputValue();

    private PD_afx_getInputValue() {
        super();
        addParameter(INPUT);
    }

    @Override
    public Value execute(final State state, final HashMap<ParameterDefinition, Value> args) throws SetlException {
        final Value inputValue = args.get(INPUT);
        if (!(inputValue instanceof Input)) {
            throw new IncompatibleTypeException("First argument '" + inputValue.toString(state) + "' is not an Input.");
        }
        final Input input = (Input)inputValue;

        return input.getValue();
    }

}
