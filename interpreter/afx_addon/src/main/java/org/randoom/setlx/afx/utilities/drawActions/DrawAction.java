package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;


/**
 * Used for describing Actions on an afx.types.Canvas
 */
public interface DrawAction {
    /**
     * Execute the defined Draw Action.
     * @param canvasWidth The current width, for resolving relative positioning.
     * @param canvasHeight The current height, for resolving relative positioning.
     * @param gc The GraphicsContext of the Canvas to be drawn onto.
     */
    void draw(double canvasWidth, double canvasHeight, GraphicsContext gc);
    
    DrawAction clone();

}
