package org.randoom.setlx.afx.types;

import javafx.scene.control.CheckBox;
import org.randoom.setlx.types.SetlBoolean;
import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.CodeFragment;
import org.randoom.setlx.utilities.State;

import java.util.Objects;

/**
 * A Wrapper Type for a JavaFX ChoiceBox
 */
public class Checkbox extends Input {
    private CheckBox javaFxControl;

    public Checkbox(CheckBox javaFxControl){
        this.javaFxControl = javaFxControl;
    }


    @Override
    public CheckBox getControl(){
        return javaFxControl;
    }

    @Override
    public Value getValue() {
        return javaFxControl.isSelected() ? SetlBoolean.TRUE : SetlBoolean.FALSE;
    }

    @Override
    public void clearValue() {
        javaFxControl.setSelected(false);
    }


    @Override
    public Value clone() {
        return new Checkbox(javaFxControl);
    }

    @Override
    public void appendString(State state, StringBuilder sb, int tabs) {
        sb.append("Checkbox");
    }

    @Override
    public int compareTo(CodeFragment other) {
        return (this.compareToOrdering() < other.compareToOrdering())? -1 : 1;
    }

    private final static long COMPARE_TO_ORDER_CONSTANT = generateCompareToOrderConstant(Checkbox.class);

    @Override
    public long compareToOrdering() {
        return COMPARE_TO_ORDER_CONSTANT;
    }

    @Override
    public boolean equalTo(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Checkbox input = (Checkbox)o;
        return Objects.equals(javaFxControl, input.javaFxControl);
    }

    @Override
    public int hashCode() {
        return javaFxControl != null ? javaFxControl.hashCode() : 0;
    }

}
