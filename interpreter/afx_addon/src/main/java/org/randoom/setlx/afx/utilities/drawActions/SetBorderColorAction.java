package org.randoom.setlx.afx.utilities.drawActions;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class SetBorderColorAction implements DrawAction {
    private final Color color;


    public SetBorderColorAction(Color color) {
        this.color = color;
    }


    @Override
    public void draw(double canvasWidth, double canvasHeight, GraphicsContext gc) {
        gc.setStroke(color);
    }

    @Override
    public DrawAction clone() {
        return new SetBorderColorAction(
            new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getOpacity())
            // As there is no real clone operation for Color, we let JavaFx create a new color with the same values.
        );
    }

}
