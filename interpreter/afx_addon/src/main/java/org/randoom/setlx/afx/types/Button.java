package org.randoom.setlx.afx.types;

import org.randoom.setlx.types.Value;
import org.randoom.setlx.utilities.CodeFragment;
import org.randoom.setlx.utilities.State;

import java.util.Objects;

/**
 * A Wrapper Type for a JavaFX Button
 */
public class Button extends Control {
    private javafx.scene.control.Button javaFxControl;

    public Button(javafx.scene.control.Button javaFxControl) {
        this.javaFxControl = javaFxControl;
    }


    @Override
    public javafx.scene.control.Button getControl(){
        return javaFxControl;
    }


    @Override
    public Value clone() {
        return new Button(javaFxControl);
    }

    @Override
    public void appendString(State state, StringBuilder sb, int tabs) {
        sb.append("Button");
    }

    @Override
    public int compareTo(CodeFragment other) {
        return (this.compareToOrdering() < other.compareToOrdering())? -1 : 1;
    }

    private final static long COMPARE_TO_ORDER_CONSTANT = generateCompareToOrderConstant(Button.class);

    @Override
    public long compareToOrdering() {
        return COMPARE_TO_ORDER_CONSTANT;
    }

    @Override
    public boolean equalTo(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Button b = (Button)o;
        return Objects.equals(javaFxControl, b.javaFxControl);
    }

    @Override
    public int hashCode() {
        return javaFxControl != null ? javaFxControl.hashCode() : 0;
    }

}
