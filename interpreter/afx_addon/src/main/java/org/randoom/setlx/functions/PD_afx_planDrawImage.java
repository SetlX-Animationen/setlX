package org.randoom.setlx.functions;

import javafx.scene.image.Image;
import org.antlr.v4.runtime.misc.Pair;
import org.randoom.setlx.afx.types.Canvas;
import org.randoom.setlx.afx.utilities.PreDefinedProcedureWithRwCanvas;
import org.randoom.setlx.afx.utilities.drawActions.DrawImageAction;
import org.randoom.setlx.exceptions.FileNotReadableException;
import org.randoom.setlx.exceptions.IncompatibleTypeException;
import org.randoom.setlx.exceptions.SetlException;
import org.randoom.setlx.parameters.ParameterDefinition;
import org.randoom.setlx.types.*;
import org.randoom.setlx.utilities.State;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

/**
 * afx_planDrawImage(canvas, fileName, tlx, tly, width, height):
 *      plans to draw an Image onto the given canvas, using Coordinates for the upper-left corner and sizes for
 *        calculating its dimensions.
 *
 *      - canvas
 *          Canvas Reference onto which the Rectangle should be drawn.
 *      - fileName
 *          String representing the file name of the Image to draw, relative to the current working directory.
 *      - tlx / tly
 *          Number, representing the $x$ and $y$ Coordinate of the upper left corner of the Rectangle.
 *      - width
 *          Number, representing the width of the Image.
 *      - height
 *          Number, representing the height of the Image.\\
 *          Defaults to being dynamically calculated to keep the Image's aspect ratio.
 *      <- \texttt{om}.
 *
 *  afx_planDrawImage(canvas, "win.png", 0.25, 0.25, 0.1);
 *  afx_planDrawImage(canvas, "loss.png", 0.25, 0.25, 0.1, 0.2);
 */
public class PD_afx_planDrawImage extends PreDefinedProcedureWithRwCanvas {
    private final static ParameterDefinition FILE_NAME   = createParameter ("fileName");
    private final static ParameterDefinition TLX         = createParameter ("tlx");
    private final static ParameterDefinition TLY         = createParameter ("tly");
    private final static ParameterDefinition WIDTH       = createParameter ("width");
    private final static ParameterDefinition HEIGHT      = createOptionalParameter ("height", Om.OM);
    public  final static PreDefinedProcedure DEFINITION  = new PD_afx_planDrawImage();

    private PD_afx_planDrawImage() {
        super();
        addParameter(FILE_NAME);
        addParameter(TLX);
        addParameter(TLY);
        addParameter(WIDTH);
        addParameter(HEIGHT);
    }

    @Override
    public Pair<Canvas, Value> executeFurther(final State state, final HashMap<ParameterDefinition, Value> args, Canvas canvas)
            throws SetlException
    {
        //region parse arguments
        final Value fileNameValue = args.get(FILE_NAME);
        if (!(fileNameValue instanceof SetlString)) {
            throw new IncompatibleTypeException("Second argument '" + fileNameValue.toString(state) + "' is not a string.");
        }
        final String fileName = state.filterFileName(fileNameValue.getUnquotedString(state));

        final Value tlxValue = args.get(TLX);
        if (!(tlxValue instanceof SetlDouble || tlxValue instanceof Rational)) {
            throw new IncompatibleTypeException("Third argument '" + tlxValue.toString(state) + "' is not a number.");
        }
        final double tlx = tlxValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tlx >= 0 && tlx <= 1)) {
            throw new IncompatibleTypeException("Third argument '" + tlxValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value tlyValue = args.get(TLY);
        if (!(tlyValue instanceof SetlDouble || tlyValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fourth argument '" + tlyValue.toString(state) + "' is not a number.");
        }
        final double tly = tlyValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(tly >= 0 && tly <= 1)) {
            throw new IncompatibleTypeException("Fourth argument '" + tlyValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value widthValue = args.get(WIDTH);
        if (!(widthValue instanceof SetlDouble || widthValue instanceof Rational)) {
            throw new IncompatibleTypeException("Fifth argument '" + widthValue.toString(state) + "' is not a number.");
        }
        final double width = widthValue.toJDoubleValue(state);
        if (canvas.isCheckBounds() && !(width >= 0 && width <= 1)) {
            throw new IncompatibleTypeException("Fifth argument '" + widthValue.toString(state) + "' is not between 0.0 and 1.0.");
        }

        final Value heightValue = args.get(HEIGHT);
        Number heightOrNull;
        if (heightValue instanceof Om) {
            heightOrNull = null;
        } else {
            if (!(heightValue instanceof SetlDouble || heightValue instanceof Rational)) {
                throw new IncompatibleTypeException("Sixth argument '" + heightValue.toString(state) + "' is not a number.");
            }
            final double heightDouble = heightValue.toJDoubleValue(state);
            if (canvas.isCheckBounds() && !(heightDouble >= 0 && heightDouble <= 1)) {
                throw new IncompatibleTypeException("Sixth argument '" + heightValue.toString(state) + "' is not between 0.0 and 1.0.");
            }
            heightOrNull = heightDouble;
        }
        //endregion

        try (
            FileInputStream fileStream = new FileInputStream(fileName)
        ) {
            Image image = new Image(fileStream);

            final double height;
            if (heightOrNull == null) {
                height = (width * (image.getHeight() / image.getWidth())) / canvas.getHeightFactor();
            } else {
                height = heightOrNull.doubleValue();
            }

            if (canvas.isCheckBounds() && !(tlx + width <= 1 && tly + height <= 1)) {
                throw new IncompatibleTypeException("Provided Sizings would be out-of-bounds.");
            }

            canvas.addDrawAction(new DrawImageAction(image, tlx, tly, width, height));

            return new Pair<>(canvas, (Value)Om.OM);
        } catch (final FileNotFoundException ex) {
            throw new FileNotReadableException("Image '" + fileName + "' does not exist.", ex);
        } catch (final IOException ex) {
            throw new FileNotReadableException("Unable to read image '" + fileName + "'.", ex);
        }
    }

}
