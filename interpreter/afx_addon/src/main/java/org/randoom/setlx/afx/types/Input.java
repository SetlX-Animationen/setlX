package org.randoom.setlx.afx.types;

import org.randoom.setlx.types.Value;

/**
 * An Abstract Base Class for Wrapper Types for JavaFX Input Controls
 */
public abstract class Input extends Control {
    public abstract Value getValue();
    public abstract void clearValue();

}
