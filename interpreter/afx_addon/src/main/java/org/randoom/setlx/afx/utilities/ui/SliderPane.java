package org.randoom.setlx.afx.utilities.ui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.util.StringConverter;

import java.text.DecimalFormat;

public class SliderPane implements Pane {
    private GridPane grid;

    private Button pauseButton;
    private ImageView playPauseButtonImageView;
    private Image pauseImage;
    private Image playImage;

    private Slider speedSlider;
    private Label speedLabel;

    private boolean isPaused;
    private final Object resumeSignaller = new Object();


    SliderPane() {
        constructGridPane();
        constructPlayPauseButton();
        constructSpeedControls();
    }


    private void constructGridPane() {
        grid = new GridPane();

        grid.setPadding(StyleConstants.onAllSidesSpacingInset);
        grid.setHgap(StyleConstants.spacingSize);
        grid.setStyle(StyleConstants.lightGrayBackgroundStyle);

        grid.setPrefHeight(StyleConstants.definitiveBottomBoxHeight);
        grid.setMaxHeight(StyleConstants.definitiveBottomBoxHeight);
        grid.setMinHeight(StyleConstants.definitiveBottomBoxHeight);

        ColumnConstraints growingWidthColCons = new ColumnConstraints();
        growingWidthColCons.setHgrow(Priority.ALWAYS);
        ColumnConstraints fixedWidthColCons = new ColumnConstraints();
        fixedWidthColCons.setHgrow(Priority.NEVER);
        grid.getColumnConstraints().addAll(
                fixedWidthColCons, new ColumnConstraints(StyleConstants.spacingSize *2),
                growingWidthColCons, new ColumnConstraints(60), fixedWidthColCons
        );

        RowConstraints fixedHeightRowCons = new RowConstraints();
        fixedHeightRowCons.setVgrow(Priority.NEVER);
        grid.getRowConstraints().add(fixedHeightRowCons);
    }

    private void constructPlayPauseButton() {
        pauseButton = new Button();

        pauseImage = new Image(getClass().getResourceAsStream("/pause.png"));
        playImage  = new Image(getClass().getResourceAsStream("/play.png"));

        playPauseButtonImageView = new ImageView();
        playPauseButtonImageView.setFitHeight(17);
        playPauseButtonImageView.setFitWidth(17);
        playPauseButtonImageView.setImage(pauseImage);
        pauseButton.setGraphic(playPauseButtonImageView);

        pauseButton.setTooltip(new Tooltip("Pause Animation"));
        isPaused = false;

        pauseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                synchronized (resumeSignaller) {
                    isPaused = !isPaused;

                    if (isPaused) {
                        playPauseButtonImageView.setImage(playImage);
                        pauseButton.setTooltip(new Tooltip("Resume Animation"));
                    } else {
                        playPauseButtonImageView.setImage(pauseImage);
                        pauseButton.setTooltip(new Tooltip("Pause Animation"));

                        resumeSignaller.notifyAll();
                    }
                }
            }
        });

        grid.add(pauseButton, 0, 0);
    }

    private void constructSpeedControls() {
        speedSlider = new Slider();
        speedSlider.setTooltip(new Tooltip("Change Speed Factor of the Animation"));

        speedSlider.setMin(0.1);
        speedSlider.setMax(1.9);
        speedSlider.setMajorTickUnit(0.15);
        speedSlider.setShowTickLabels(true);
        speedSlider.setSnapToTicks(false);

        final DecimalFormat sliderValueFormatter = new DecimalFormat("#0.00");
        speedSlider.setLabelFormatter(new StringConverter<Double>() {
            @Override public String toString(Double n) {
                return sliderValueFormatter.format(formatSpeedSliderValue(n));
            }
            @Override public Double fromString(String s) {
                double n = Double.parseDouble(s);
                if (n <= 1) {
                    return n;
                } else {
                    return (n - 1) / 10 + 1;
                }
            }
        });

        speedSlider.setValue(1.0);
        grid.add(speedSlider, 2, 0);

        speedLabel = new Label(" 1.00x");
        speedSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                speedLabel.setText(" " + sliderValueFormatter.format(getSpeedFactor()) + "x");
            }
        });
        grid.add(speedLabel, 3, 0);

        Button resetButton = new Button();
        resetButton.setTooltip(new Tooltip("Reset Speed Factor to 1.0"));

        ImageView resetView = new ImageView();
        resetView.setFitHeight(17);
        resetView.setFitWidth(17);
        resetView.setImage(new Image(getClass().getResourceAsStream("/reset_slider.png")));
        resetButton.setGraphic(resetView);

        resetButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                speedSlider.setValue(1.0);
            }
        });

        grid.add(resetButton, 4, 0);
    }


    @Override
    public Node getNode() {
        return grid;
    }

    @Override
    public double getWidth() {
        return grid.getWidth();
    }

    @Override
    public double getHeight() {
        return grid.getHeight();
    }


    private double formatSpeedSliderValue(double rawValue) {
        if (rawValue < 1) {
            return rawValue >= 0.1 ? rawValue : 0.1;
        } else {
            return 1 + (rawValue - 1) * 10;
        }
    }

    public double getSpeedFactor() {
        return formatSpeedSliderValue(speedSlider.getValue());
    }


    /**
     * If the current state is paused, this method blocks until its resumed.
     * If it is currently not paused, it returns immediately.
     */
    public synchronized void waitIfPaused() {
        synchronized (resumeSignaller) {
            if (isPaused) {
                try {
                    resumeSignaller.wait();
                } catch (InterruptedException e) {
                    waitIfPaused();
                }
            }
        }
    }

}
