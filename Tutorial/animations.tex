%!TEX root = tutorial.tex

\newcommand{\fullref}[1]{\autoref{#1} on page \pageref{#1}}

\chapter{Animations}
\setlx \ also provides a new graphical interface through the Java-FX library. It is meant to be used for animations or in general graphical interfaces. Every function of AFX starts with the prefix ``\texttt{afx\_}''.\\
It was implemented by Mauricio Abrigada, Joshua T\"{o}pfer, and Luca Vazzano.

\section{Meet the canvas}

A canvas is the place where elements are drawn on. Therefore every element should get a canvas on which it should be drawn.

If two elements are drawn on the same canvas, they are drawn in the order they were called. That means, if we have two elements and element one is called first, it is drawn at first and after it element two. You should therefore carefully think about the order in which you call the elements. Additionally every element that should be drawn onto a canvas is first planned to be drawn. With the command ``\texttt{afx\_draw(canvas)}'' all planned elements will be drawn onto the canvas.

\section{Drawing simple Forms}

In this section, we discuss how to create simple forms with AFX. How the result should look like can be seen in \fullref{fig:afx_example1.eps} and the code can be seen in \fullref{fig:afx_example1.stlx} and \fullref{fig:afx_example1.2.stlx}.

\begin{enumerate}
\item At first we create the window with
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_showWindow("DrawTest");}
\\[0.2cm]
and name it after the string inside the brackets. As default, it is named ``\texttt{SetlX - AFX Output}''.
\item After this, we create a canvas. The \texttt{1} describes the ratio between width and height, in this case it's 1:1. With a \texttt{2/3}, the canvas has the ratio of 3:2 as width to height. The same is possible in the other direction with \texttt{3/2}. The ratio there is 2:3 as width to height.\\
The function returns the canvas, which should be stored in a variable. This is needed because the canvas needs to be passed to procedures which plan drawings onto the canvas. Crucially, the canvas needs to be passed to them with the prefix \texttt{rw}. This prefix gives the procedure read-write rights on the canvas, otherwise only a copy will be passed to the procedure and the added elements of this procedure wouldn't be remembered, as that copy gets deleted once the procedure-call finishes.\\
It is possible to use more than one canvas.
\item In line 5 and 6 we create a black background. To achieve this, we just draw a rectangle that has the size of the canvas.\\
In line 6 the rectangle is created with the coordinates \texttt{<0,0>} and \texttt{<1,1>}. These are the top left edge (\texttt{<0,0>}) and the bottom right edge (\texttt{<1,1>}) of the canvas. Additionally these coordinates are relative. In AFX, relative measurements or coordinates express that the maximum number can only be 1 and they are scaled depending on the actual size of the canvas. For example, if the canvas would be 500 pixels wide and the coordinate would be \texttt{0.2}, then the coordinate would be at 100 pixels from the left. But if the canvas would be 1000 pixels wide, then the coordinates would be at 200 pixels from the left.\\
In line 5 we set the color of the rectangle. The command sets every element which is created after the command on the same canvas to the defined color. But it only sets the color of the element's fill. The border color can be set with another command, which you can see in line 13.
\item In lines 9 to 12 we define the position of the top left edge and the width and height of another rectangle, that is defined in line 14. Here we create the rectangle with the width and height.\\
This rectangle only has a border and isn't filled like the background. This can be defined through the boolean values at the end of the command. The first value defines if the element should be filled and the second defines if the element should have a border.\\
There is also another argument that can be specified, which defines if the border should be \texttt{inside}, \texttt{middle} or \texttt{outside} of the rectangle. If it's \texttt{inside}, the border won't extend further then the dimensions of the rectangle. If it's \texttt{middle}, the border will be half inside and half outside the rectangle. Finally if it is \texttt{outside}, the full width of the border will be added to the rectangle on all sides. This should be considered when adding a border to elements close to each others. The default value is \texttt{middle}.
\item In line 13, we set the color of the border or of lines in general on the specified canvas to \texttt{DARKBLUE}.

\vspace{0.7cm}

\begin{figure}[!h]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    afx_showWindow("DrawTest");
    canvas := afx_addCanvas(1);

    // set background to black
    afx_planSetFillColor(canvas, "BLACK");
    afx_planDrawRectWithCoords(canvas, 0, 0, 1, 1);

    // draw a rectangle
    upperLeftX := 1/4;
    upperLeftY := 1/4;
    width      := 1/2;
    height     := 3/8;
    afx_planSetLineColor(canvas, "DARKBLUE");
    afx_planDrawRectWithSizes(canvas, upperLeftX, upperLeftY, width,
      height, false, true);

    // draw an ellipse enclosed in the rectangle
    afx_planSetFillColor(canvas, "YELLOW");
    afx_planDrawEllipse(canvas, upperLeftX, upperLeftY, width, height);
\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show simple forms. Part 1.}
\label{fig:afx_example1.stlx}
\end{figure}
\clearpage

\item In line 19 we create an ellipse with the exact same variables. You can see the result in \fullref{fig:afx_example1.eps} as the filled yellow ellipse. The ellipse has the same optional arguments like the rectangle, as you can see in line 32 in \fullref{fig:afx_example1.2.stlx}.
\item As the next step, in lines 21 to 26, we draw two lines across the rectangle we created in line 14. The lines get the start and end points as relative x and y coordinates.
\item In lines 28 to 30 we define the center point of the canvas and a radius. We use these variables to create a circle. This circle is not filled and only has a border.
\item Additionally we set the width of the border to 4. This means that every line or border which is drawn after this command will be 4 pixels wide.
\item Now in line 37 we use another command to set the color manually, affecting the ellipse dranw afterwards in line 38. The other colors we used were predefined and can be found in \fullref{fig:predefined-color-names}.\\
But this time we give the command red, green, blue and alpha values. These must be in the interval \texttt{[0,255]}, except for the alpha value, which must be in the interval \texttt{[0,1]}. The alpha value defines the transparency of the element, that is drawn with this color.\\

\vspace{1cm}

\begin{figure}[!hb]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 20,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    // draw a diagonal line
    afx_planSetLineColor(canvas, "MAGENTA");
    afx_planDrawLine(canvas, upperLeftX, upperLeftY, upperLeftX + width,
      upperLeftY + height);
    afx_planSetLineColor(canvas, "BLACK");
    afx_planDrawLine(canvas, upperLeftX, upperLeftY + height,
      upperLeftX + width, upperLeftY);

    centerX := upperLeftX + width  / 2;
    centerY := upperLeftY + height / 2; 
    radius  := 0.5 * sqrt(width * width + height * height);
    afx_planSetLineColor(canvas, "LAWNGREEN");
    afx_planSetLineWidth(canvas, 4);
    afx_planDrawEllipse(canvas, centerX - radius, centerY - radius,
      2 * radius, 2 * radius, false, true);

    //draw another transparent circle
    afx_planSetFillColorRGBA(canvas, 0, 0, 255, 0.6);
    afx_planDrawEllipse(canvas, upperLeftY - 0.5 * radius,
      upperLeftY - 0.5 * radius, radius, radius);

    afx_draw(canvas,0);

    // keeps window open and prevents that setlx process gets killed
    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show simple forms. Part 2.}
\label{fig:afx_example1.2.stlx}
\end{figure}
\clearpage

\item Every command we used to draw something onto the canvas was actually just planned to be drawn. In order to actually draw these elements onto the canvas, we need to use the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_draw(canvas, ms)}
\\[0.2cm]
as seen in line 41. This command actually draws it in the order in which the elements were called. The \texttt{ms} argument specifies how much time should pass after the elements are drawn onto the canvas, and the next possible draw call can be run. This is helpful if a status should be longer visible to the user. Additionally this number is manipulated by the slider at the bottom of the window to allow the user to adapt the speed at which the actions displayed by the program are happening to their needs.
\item The last command in line 44 is needed to prevent \setlx \ from closing the window after it computed every command inside the file.\\
If these commands are executed through \setlx{}'s interactive mode without using a file, the window stays open even without the last command.
\end{enumerate}

\begin{figure}[!hb]
  \centering
  \epsfig{file=afx_example1.eps, scale=0.4}
  \caption{The basic elements.}
  \label{fig:afx_example1.eps}
\end{figure}

\section{Drawing complex Elements}

In the previous section we created simple elements, like rectangles and circles. In this section we concentrate on more complex elements, like images and text. The code for this can be seen in \fullref{fig:afx_example2.stlx}.

\begin{enumerate}
\item At the beginning we define the window and a canvas. After this, in line 5, we tell \setlx \ to avoid checking if an element that is going to be drawn onto the canvas really fits there. This is needed because the sinus wave is wider than the canvas.\\
Next we draw the image in line 7. The specified arguments are the canvas on which the image will be drawn, the path of the image, the x and y coordinate of the top left edge of the image on the canvas and the width of the image as a relative value.\\
There is also the option to specify the height of the image, but by not setting it explicitly we tell \setlx \ to calculate the height according to the given width and the ratio of the original image-file's width to height.
\item In line 10 we define a new text size and a new font family. The text size is in \texttt{pt} and to set the font family it is required to give the path to the font file.
\item As next step in line 11 we need to set the fill color to \texttt{BLACK}, as the text element is white by default and can have a border, which is why it is a filled element.
\item In line 12 we draw the text. The second argument is the string representing the text itself. The third and fourth arguments are the start point from which the text will be drawn and the last argument specifies the alignment of the text. There are different options to choose from: \texttt{LEFT}, \texttt{CENTER} and \texttt{RIGHT}.
\item In line 15 we draw another image. This time we specify the width bigger than \texttt{1} which would cause a part of the image to be drawn outside the canvas. As we disabled the checking and prevention of this earlier, the image drawn will just be chopped off at the edge of the canvas, without an error message.
\item At last we draw the elements and keep the window open, to see our result.
\end{enumerate}

The result can be seen in \fullref{fig:afx_example2.eps}.

\vspace{1cm}

\begin{figure}[hb]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    afx_showWindow("DrawTest");
    canvas := afx_addCanvas(1);

    afx_setCheckBounds(canvas, false);

    // draw an image
    afx_planDrawImage(canvas, "resources/heraklit.gif", 0, 0, 32/50);

    // draw text in a specified font
    afx_planSetTextStyle(canvas, 24, "resources/greekfp.ttf");
    afx_planSetFillColor(canvas, "BLACK");
    afx_planDrawText(canvas, "panta rei", 0.68, 0.38, "LEFT");

    // draw an image with transparency
    afx_planDrawImage(canvas, "resources/sinus-wave.png", 0, 1 - 0.286, 2);
    afx_draw(canvas, 50);

    // keeps window open and prevents that setlx process gets killed
    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to use pictures and font.}
\label{fig:afx_example2.stlx}
\end{figure}
\clearpage

\begin{figure}[ht]
  \centering
  \epsfig{file=afx_example2.eps, scale=0.4}
  \caption{The complex elements.}
  \label{fig:afx_example2.eps}
\end{figure}

\section{Animations}

After we have seen how to implement basic and complex elements with \setlx \ we can now animate the elements. We use the code from \fullref{fig:afx_example2.stlx} and add a loop, which draws the image of the wave every time one bit further to the left. The resulting, full code can be seen in \fullref{fig:afx_example3.stlx}.

\begin{enumerate}
\item The new variable \texttt{steps} in line 6 creates a list with only even numbers from 2 to 284. 284 is exactly the x coordinate in the picture at which one sinus wave ends. That's why the picture resets itself at this point, to create the illusion of the wave being infinite.
\item The loops in lines 8 and 9 are for the infinite repetition of drawing the sinus wave. Every time the for-loop finishes, the while-loop restarts it. The for-loop draws the sinus wave in every step that is defined in \texttt{steps}.
\item In line 10, we use the 
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_planClearCanvas(canvas)}
\\[0.2cm]
command to clear the canvas at the start of every drawing. This is needed to prevent drawing the new elements on top of and over the old elements which were drawn with the previous cycle. This is not desirable for this program and therefore prevented through using this command.
\item To actually move the image we set the start \texttt{x}-coordinate every step a bit further to the left, until there are no steps left. Then we start again all over from the beginning.
\end{enumerate}

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    afx_showWindow("DrawTest");
    canvas := afx_addCanvas(1);

    afx_setCheckBounds(canvas, false);

    steps := [2, 4 .. 284];

    while(true){
      for(index in steps){
        afx_planClearCanvas(canvas);
        // draw an image
        afx_planDrawImage(canvas, "resources/heraklit.gif", 0, 0, 32 / 50);

        // draw text in a specified font
        afx_planSetTextStyle(canvas, 24, "resources/greekfp.ttf");
        afx_planSetFillColor(canvas, "BLACK");
        afx_planDrawText(canvas, "panta rei", 0.68, 0.38, "LEFT");

        // draw an image with transparency
        afx_planDrawImage(canvas, "resources/sinus-wave.png",
          0 - index / 1000, 1 - 0.286, 2);
        afx_draw(canvas, 50);
      }
    }

    // keeps window open and prevents that setlx process gets killed
    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show a simple animation.}
\label{fig:afx_example3.stlx}
\end{figure}

\section{Interacting with the user through inputs}

After learning about how to get a simple animation, we focus on input options. With inputs you can manipulate the animation according to the user's wishes without changing the code.\\
The next example will create a moving sun around the planet earth. It is possible to change the speed of the sun at the beginning, which defines how many milliseconds should be waited until the sun moves one step further. The code for this can be seen in \fullref{fig:afx_example4.stlx} and  \fullref{fig:afx_example4.2.stlx}.

\begin{enumerate}
\item The main procedure is \texttt{startAnimation} which draws the animation. The procedure gets a canvas and an input value from a text field. The text field is defined in line 44 with the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_addTextInput();}
\\[0.2cm]
This command creates a text field inside the left part of the animation window, as you can see in \fullref{fig:afx_example4.eps}. It also returns a text field object, which needs to be saved into a variable to later read the user's input into the created input field.
\item In line 2 the value of the input field is read into the variable \texttt{delay}. To accomplish this we use the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_getInputValue(input);}
\\[0.2cm]
This command returns the text inside the input field as a string.\\
Therefore, if we want to process it as a number, we have to convert it to an integer by using the command ``\texttt{int(string)}'' which parses the input to a number.
\item Afterwards we define further variables with values which are needed for the animation.
\item In lines 10 and 11 we create two loops to animate the drawing.
\item After this, we clear the canvas and draw the earth and the sun with different positions. The delay that we defined at the start will set the delay the command ``\texttt{afx\_draw(canvas, ms)}'' uses. With this, the \texttt{startAnimation}-procedure is finished.
\item In line 40 we create a ``control header'' that represents a headline.\\
Similarly, in line 42 we set a control hint, which is basically just a smaller headline. These can be used to present further information about input fields to the user.
\item In line 45 we create a button that should start the animation. For this we use the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_addButton("start", [canvas, input], startAnimation, "ENTER");}
\\[0.2cm]
The arguments of this are at first the text on the button, second a list of arbitrary variables which will be passed to the provided function once the button is clicked, third the reference to that function and finally the keyboard-key that optionally activates the button, in form of a keycode. The button of course can also be clicked with the mouse.
\end{enumerate}

The complete program can be seen in \fullref{fig:afx_example4.eps}.

\vspace{0.5cm}

\begin{figure}[!hb]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    startAnimation := procedure(canvas, input){
      delay := int(afx_getInputValue(input));
      centerEarthX := 1/2;
      centerEarthY := 1/2;
      radiusEarth  := 1/10;
      orbitEarth   := 0.35;
      radiusSun    := 1/20;
      redSun       := 255;
      blueSun      := 0;
      while(true){
        for(angle in [0, 1/5 .. 359]){
          afx_planClearCanvas(canvas);

          //calculate values need for drawing the sun
          centerSunX := centerEarthX + orbitEarth * cos(angle);
          centerSunY := centerEarthY + orbitEarth * sin(angle);
          greenSun := (0.5 * (1 - sin(angle))) * 255;

          // draw the earth
          afx_planSetFillColor(canvas, "BLUE");
          afx_planDrawCircle(canvas, 
            centerEarthX - radiusEarth, 
            centerEarthY - radiusEarth, 2 * radiusEarth);

\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show an animation with input options, part 1.}
\label{fig:afx_example4.stlx}
\end{figure}
\clearpage

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 25,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
          // draw the sun
          afx_planSetFillColorRGBA(canvas, redSun, greenSun, blueSun, 1);
          afx_planDrawCircle(canvas, 
            centerSunX - radiusSun, 
            centerSunY - radiusSun, 2 * radiusSun);

          afx_draw(canvas, delay);
        }
      }
    };

    // STARTUP
    afx_showWindow("Planet Animation");
    canvas := afx_addCanvas(1);

    afx_addControlHeader("Planet Animation");

    afx_addControlHint("Enter delay between frames (in ms):");
    input := afx_addTextInput();

    afx_addButton("start", [canvas, input], startAnimation, "ENTER");

    // keeps window open and prevents that setlx process gets killed
    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show an animation with input options, part 2.}
\label{fig:afx_example4.2.stlx}
\end{figure}

\begin{figure}[!hb]
  \centering
  \epsfig{file=afx_example4.eps, scale=0.34}
  \caption{An animation of two circles.}
  \label{fig:afx_example4.eps}
\end{figure}

It is also possible to use checkboxes and dropdown menus. You can see the code for this in \fullref{fig:afx_example5.stlx} and \fullref{fig:afx_example5.2.stlx} with its output in \fullref{fig:afx_example5.eps}.

\begin{enumerate}
\item In lines 1 to 19 we declare different procedures for our buttons, which themselves are defined in lines 44 to 51. The procedures do the following:
\vspace{-0.15cm}
\begin{itemize}
\item ``\texttt{doNothing}'' does, as the name says, nothing.
\item ``\texttt{clearInputs}'' clears every input field to the initial value with the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_clearInputValue(input)}
\vspace{0cm}
\item ``\texttt{drawState}'' simlpy draws the current value of every input onto the canvas.
\end{itemize}
\item In line 33 we define different options which will be used in the dropdown menu. The dropdown menu will be created in the next line and gets the list of choices.
\item In line 38 we create a checkbox which gets a name as argument. If the checkbox is activated it returns the value \texttt{true}, otherwise it returns the value \texttt{false}.
\item In line 41 we set the default focus to the input field from line 29. This enables the user to type into the field right away when they start the program, without having to click it first.
\item In line 47 we group two buttons together by using the command 
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_groupButtons(list\_of\_buttons)}
\\[0.2cm]
Its first argument is the list of buttons which should be grouped together. When we group buttons they will be aligned directly besides each other and not underneath each other, thus visually forming a group of related actions.
\item We can also disable controls, i.e. a button, as can be seen in line 52 with the command \\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_setControlDisabled(button, true)}
\\[0.2cm]
It gets the control and a boolean which defines if it should be disabled (\texttt{true}) or enabled (\texttt{false}).

\begin{figure}[!h]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    doNothing := procedure() {};

    clearInputs := procedure(canvas, inputList) {
      for(input in inputList){
        afx_clearInputValue(input);
      }
      drawState(canvas, inputList);
    };

    drawState := procedure(canvas, inputList) {
      if(inputList != []){
        for(inputIndex in [1..#inputList]){
          afx_planDrawText(canvas, "$inputList[inputIndex]$: 
            $afx_getInputValue(inputList[inputIndex])$", 
            0.5, 0.8 / #inputList * inputIndex);
        }
        afx_draw(canvas,0); 
      }
    };

\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show more input options, part 1.}
\vspace*{-1cm}
\label{fig:afx_example5.stlx}
\end{figure}

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 21,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    // STARTUP
    afx_showWindow("Example Inputs");
    canvas := afx_addCanvas(1);

    afx_addControlHeader("Example Inputs");

    // Add section for text input
    afx_addControlHint("This is a text input:");
    textInput := afx_addTextInput();

    // Add section for dropdown
    afx_addControlHint("This is a dropdown:");
    choices := ["random", "sorted ascending", "sorted descending"];
    dropdown := afx_addChoiceDropdown(choices);

    // Add section for checkbox
    afx_addControlHint("This is a checkbox:");
    checkbox := afx_addCheckbox("Checkbox");

    //focus the text input such that the user can directly start writing
    afx_focusInput(textInput);
  
    // add buttons for draw and clear and group them
    drawButton  := afx_addButton("draw", [canvas,
      [textInput, dropdown, checkbox]], drawState, "ENTER");
    clearButton := afx_addButton("clear", [canvas,
      [textInput, dropdown, checkbox]], clearInputs);
    afx_groupButtons([drawButton, clearButton]);

    // add useless button and disable input
    uselessButton := afx_addButton("useless", om , doNothing);
    afx_setControlDisabled(uselessButton, true);

    // keeps window open and prevents that setlx process gets killed
    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{\setlx\ commands to show more input options, part 2.}
\label{fig:afx_example5.2.stlx}
\end{figure}

\end{enumerate}
\clearpage

\begin{figure}[t]
  \centering
  \epsfig{file=afx_example5.eps, scale=0.4}
  \caption{Some possible inputs.}
  \label{fig:afx_example5.eps}
\end{figure}

\section{Additional content}

In this section we discuss some additional neat tricks and good to know information.

\subsection{Defining colors in different ways}

In \fullref{fig:afx_example6.eps} we see texts that were colored with different commands. The code can be seen in \fullref{fig:afx_example6.stlx}. There are three different methods to set the used colors:

\begin{enumerate}
\item ``\texttt{afx\_planSetFillColor(canvas, color)}'' and ``\texttt{afx\_planSetLineColor(canvas, color)}'' use predefined color-names. These can be found in \fullref{fig:predefined-color-names}. The commands can be seen in lines 7 and 8.
\item Otherwise ``\texttt{afx\_planSetFillColorRGBA(canvas, red, green, blue, alpha)}'' as well as ``\texttt{afx\_planSetLineColorRGBA(canvas, red, green, blue, alpha)}'' use the RGBA color-model, which uses a red, a green, a blue and an alpha value to define the color. Red, green and blue can be between 0 and 255, while the alpha value can only be between 0 and 1 and defines the opacity of the color. These commands can be found in lines 13 and 14.
\item Similarly ``\texttt{afx\_planSetFillColorHSVA(canvas, hue, saturation, value, alpha)}'' and ``\texttt{afx\_planSetLineColorHSVA(canvas, hue, saturation, value, alpha)}'' use the HSVA color-model, which is visualized in Figure \fullref{fig:hsv-color-model}. It uses a hue, a saturation, a value and an alpha value. The hue value can be between 0 and 360, while the other values can only be between 0 and 1. These commands can be found in lines 19 and 20.
\end{enumerate}

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    afx_showWindow("Example Colors");
    canvas := afx_addCanvas(1);

    afx_planSetTextStyle(canvas, 36);

    // text with color defined by name for fill and outline
    afx_planSetFillColor(canvas, "BLUE");
    afx_planSetLineColor(canvas, "BLACK");
    afx_planDrawText(canvas, "color defined by colorname", 0.5, 1/4,
      "CENTER", true, true);

    // text with color defined by rgb for fill and outline
    afx_planSetFillColorRGBA(canvas, 127, 0, 255, 1);
    afx_planSetLineColorRGBA(canvas, 255, 0, 0, 1);
    afx_planDrawText(canvas, "color defined by rgb values", 0.5, 2/4,
      "CENTER", true, true);

    // text with color defined by hsv for fill and outline
    afx_planSetFillColorHSVA(canvas, 180, 1, 1, 1);
    afx_planSetLineColorHSVA(canvas, 260, 0.7, 0.7, 1);
    afx_planDrawText(canvas, "color defined by hsv values", 0.5, 3/4,
      "CENTER", true, true);

    afx_draw(canvas, 0);

    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{Different ways to define colors in \setlx .}
\label{fig:afx_example6.stlx}
\end{figure}

\begin{figure}[!hb]
  \centering
  \epsfig{file=afx_example6.eps, scale=0.3}
  \caption{Different coloring methods.}
  \label{fig:afx_example6.eps}
\end{figure}

\subsection{Relative text size}

In this section we see how to scale text depending on the size of the canvas. This is needed as the text's size can't accurately be scaled automatically, unlike the other relative measurements (e.g. for the sizes of rectangles). Therefore, we need to calculate the size ourselves. The program in \fullref{fig:afx_example6.stlx} basically just draws text onto the canvas. To get the size of the canvas the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_getCanvasWidth(canvas)}
\\[0.2cm]
is used. It returns the width of the canvas in pixel as a number. This can be used with the initial width of the canvas to get the scaling factor, which can be seen in line 2.

The full program can be seen in \fullref{fig:afx_example7.eps}.

\begin{figure}[!h]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    drawText := procedure(canvas, originalCanvasWidth) {
      afx_planSetTextStyle(canvas,
        72 / originalCanvasWidth * afx_getCanvasWidth(canvas));
      afx_planSetFillColor(canvas, "Black");
      afx_planDrawText(canvas, "some text", 0.5, 0.5);
      afx_draw(canvas, 500);
    };

    // STARTUP
    afx_showWindow("Example Relative Text Size");
    canvas := afx_addCanvas(1);
    originalCanvasWidth := afx_getCanvasWidth(canvas);

    while(true){
      drawText(canvas, originalCanvasWidth);
    }
\end{Verbatim}
\vspace*{-0.3cm}
\caption{Relative text sizes in \setlx .}
\label{fig:afx_example7.stlx}
\end{figure}

\begin{figure}[!hb]
  \centering
  \epsfig{file=afx_example7.eps, scale=0.3}
  \caption{Relative text size.}
  \label{fig:afx_example7.eps}
\end{figure}

\subsection{Modals and sound}

There are also modals, which you can see in \fullref{fig:afx_example9.eps}, which are message-boxes overlaying the whole animation output. They can be used to inform the user about various things in an eye-catching style.\\
The command for a modal is
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_showModal(title, text, list\_with\_buttons)}
\\[0.2cm]
The arguments are a title, a text and a list with button names for the modal. The modal returns the index of the button which was pressed.\\
Similarly, the command can also be called like
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_showModal(title, text, list\_with\_buttons, is\_error)}
\\[0.2cm]
where the argument \texttt{is\_error} is a boolean value. If \texttt{true}, it produces a red flash in the background of the modal, clearly indicating that an error occured. If it is \texttt{true} or left out, the modal defaults to a static grey background.\\

There is also the option to implement sound with the command
\\[0.2cm]
\hspace*{1.3cm}
\texttt{afx\_playSound(path\_to\_soundfile)}
\\[0.2cm]
The only argument is the path to the soundfile that should be played.\\

The command to create a window also has the optional argument to let the user confirm the close of the window. You can see this in line 2 of \fullref{fig:afx_example_some_more_stuff.stlx}.

\vspace{2.5cm}

\begin{figure}[h]
\centering
\begin{Verbatim}[ frame         = lines,
                  framesep      = 0.3cm,
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    // Add window with exit confirmation
    afx_showWindow("Examples",true);
    canvas := afx_addCanvas(1);

    // add button that plays a sound
    afx_addButton("play sound",["resources/sound.mp3"],afx_playSound);

    // add button that shows a modal
    afx_addButton("show modal", ["some modal","some text here...",["OK"]],
      afx_showModal);

    // keeps window open and prevents that setlx process gets killed
    afx_switchToEventProcedures();
\end{Verbatim}
\vspace*{-0.3cm}
\caption{More examples in \setlx .}
\label{fig:afx_example_some_more_stuff.stlx}
\end{figure}

\begin{figure}[h]
  \centering
  \epsfig{file=afx_example8.eps, scale=0.35}
  \caption{Modal and sound buttons.}
  \label{fig:afx_example8.eps}
\end{figure}

\begin{figure}[h]
  \centering
  \epsfig{file=afx_example9.eps, scale=0.35}
  \caption{A modal.}
  \label{fig:afx_example9.eps}
\end{figure}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tutorial.tex"
%%% End:
